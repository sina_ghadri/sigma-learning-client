
export class User {
    id: number;
    name: string;
    username?: any;
    email?: any;
    cellphone?: any;
    birthday?: any;
    about: string;
    sendEmail: boolean;
    sendMessage: boolean;
    links?: any;
    creationTime: string;
}