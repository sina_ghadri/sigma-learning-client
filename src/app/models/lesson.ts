import { Course } from "./course";
import { Post } from "./post";

export class Lesson {
  id: number;
  post: Post;
  time: number;
  index: number;
  course: Course;
  url: string;
  isFree: boolean;
}