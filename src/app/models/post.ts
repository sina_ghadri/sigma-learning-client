import { User } from "./user";
import { PostImage } from "./image";
import { KeyValue } from "sigmasoft-ts";
import { Router } from "@angular/router";
import { LessonService } from "../services/lesson.service";

export class Post {
    id: number;
    symbol: string;
    title: string;
    description: string;
    body: string;
    images?: PostImage[];
    rateP: number;
    rateN: number;
    category?: any;
    user: User;
    nextPost?: any;
    prevPost?: any;
    creationTime: Date;
    parent?: any;
    viewCount: number;
    publishTime?: any;
    type: KeyValue;
    tags?: any;
    links?: any;

    get link(): string {
        switch (this.type.key) {
            case PostType.Article: return 'article';
            case PostType.Course: return 'course';
            case PostType.Lesson: return 'course';
            case PostType.News: return 'news';
            case PostType.Page: return 'page';
        }
        return 'post';
    }
    goto(router: Router, lessonService?: LessonService) {
        let link = this.link;
        if (lessonService && this.type.key == PostType.Lesson) {
            lessonService.getLessonById(this.id).subscribe(op => {
                if (op.success) router.navigate([link, op.data.course.id], { queryParams: { lesson: op.data.id } });
            })
        }
        else {
            router.navigate([link, this.id]);
        }
    }
}

export enum PostType {
    Article = 1,
    Course = 2,
    Lesson = 3,
    News = 4,
    Page = 5,
}