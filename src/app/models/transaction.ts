export class Transaction {

}

export enum TransactionStatus {
    Draft = 0,
    SendToPortal = 1,
    Verifying = 2,
    Verified = 3,
    PaidSuccessfull = 4,
    Cancel = 5
}
export enum TransactionType {
    Course = 1,
    VipService = 2
}