import { KeyValue } from "sigmasoft-ts";
import { Post } from "./post";
import { User } from "./user";


export class Course {
  id: number;
  post: Post;
  teacher: User;
  status: KeyValue;
  time: number;
  type: KeyValue;
  price: number;
  isbought: boolean;
}

export enum CourseType {
  Free = 0,
  Vip = 1,
  OnSale = 2,
}