
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OperationResult } from 'sigmasoft-ts';
import { Article } from '../models/article';
import { ConfigurationService } from 'sigmasoft-ng';
@Injectable()
export class ArticleService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getArticles(): Observable<OperationResult<Article[]>> {
    return this.http.get<OperationResult<Article[]>>(this.config.api.article);
  }
  getArticleById(id: number): Observable<OperationResult<Article>> {
    return this.http.get<OperationResult<Article>>(this.config.api.article + id);
  }
}
