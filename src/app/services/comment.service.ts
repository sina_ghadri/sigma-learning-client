
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OperationResult } from 'sigmasoft-ts';
import { ConfigurationService } from 'sigmasoft-ng';

@Injectable()
export class CommentService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getCommentByParentId(id: number): Observable<OperationResult<any[]>> {
    return this.http.get<OperationResult<any[]>>(this.config.api.comment + id);
  }
}
