
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OperationResult } from 'sigmasoft-ts';
import { TransactionType } from '../models';
import { ConfigurationService } from 'sigmasoft-ng';

@Injectable()
export class TransactionService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  pay(obj: { referenceId: number, type: TransactionType, callbackUrl?: string }): Observable<OperationResult<string>> {
    obj.callbackUrl = this.config.payment.callbackUrl;
    return this.http.post<OperationResult<string>>(this.config.api.transaction, obj);
  }
  veify(obj: { authority: string, isOk: boolean }): Observable<OperationResult<any>> {
    return this.http.put<OperationResult<any>>(this.config.api.transaction, obj);
  }
}
