
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OperationResult } from 'sigmasoft-ts';
import { Course, Lesson } from '../models';
import { ConfigurationService } from 'sigmasoft-ng';

@Injectable()
export class CourseService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getCourses(): Observable<OperationResult<Course[]>> {
    return this.http.get<OperationResult<Course[]>>(this.config.api.course);
  }
  getCourseById(id: number): Observable<OperationResult<{ course: Course, lessons: Lesson[] }>> {
    return this.http.get<OperationResult<{ course: Course, lessons: Lesson[] }>>(this.config.api.course + id);
  }
}
