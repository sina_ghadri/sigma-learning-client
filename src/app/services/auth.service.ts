import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KeyValue, OperationResult } from 'sigmasoft-ts';
import { ConfigurationService, HttpConfigService } from 'sigmasoft-ng';
import { Observable } from 'rxjs';

@Injectable()
export class AuthService {
  private pretoken: string;
  private _islogin: boolean;
  public refrenceUrl: string;

  get token() {
    return localStorage.getItem('token');
  }
  set token(value: string) {
    if (value) localStorage.setItem('token', value);
    else localStorage.removeItem('token');
    this.httpConfig.token = value;
  }
  get islogin(): boolean {
    let token = this.token;
    if (this.pretoken != token) {
      this.pretoken = token;
      this.checklogin().subscribe();
    }
    if (this.httpConfig.token) {
      return this._islogin;
    }
    return false;
  }
  set islogin(value: boolean) {
    this._islogin = value;
  }

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService, private httpConfig: HttpConfigService) { }

  checklogin(): Observable<boolean> {
    return new Observable(ob => {
      this.http.get<OperationResult<any>>(this.config.api.auth).subscribe(op => {
        this.islogin = op.success;
        ob.next(this.islogin);
      }, e => {
        this.islogin = false;
        ob.next(this.islogin);
      });
    });
  }
  login(obj: { username: string, password: string, captcha: KeyValue }): Observable<OperationResult<string>> {
    return this.http.post<OperationResult<string>>(this.config.api.auth, obj);
  }
  logout() {
    this.http.delete<OperationResult<any>>(this.config.api.auth).subscribe();
    this.token = null;
  }
  register(obj: { name: string, email: string, password: string, captcha: KeyValue }): Observable<OperationResult<string>> {
    return this.http.put<OperationResult<string>>(this.config.api.auth, obj);
  }
  reset(obj: { username: string, captcha: KeyValue }): Observable<OperationResult<any>> {
    return this.http.put<OperationResult<any>>(this.config.api.auth + 'reset', obj);
  }
}
