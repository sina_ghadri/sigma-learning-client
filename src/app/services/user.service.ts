import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OperationResult } from 'sigmasoft-ts';
import { ConfigurationService } from 'sigmasoft-ng';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getProfile(): Observable<OperationResult<any>> {
    return this.http.get<OperationResult<any>>(this.config.api.user);
  }
  updateProfile(obj: any): Observable<OperationResult<any>> {
    return this.http.patch<OperationResult<any>>(this.config.api.user, obj);
  }
  changePassword(obj: any): Observable<OperationResult<any>> {
    return this.http.patch<OperationResult<any>>(this.config.api.user + 'password',obj);
  }
  getVipServices(): Observable<OperationResult<any>> {
    return this.http.get<OperationResult<any>>(this.config.api.user + 'vipservices');
  }
  getTransactions(): Observable<OperationResult<any[]>> {
    return this.http.get<OperationResult<any[]>>(this.config.api.user + 'transactions');
  }
  getCourses(): Observable<OperationResult<any[]>> {
    return this.http.get<OperationResult<any[]>>(this.config.api.user + 'courses');
  }
  getBookmarks(): Observable<OperationResult<any[]>> {
    return this.http.get<OperationResult<any[]>>(this.config.api.user + 'bookmarks');
  }
  getLikes(): Observable<OperationResult<any[]>> {
    return this.http.get<OperationResult<any[]>>(this.config.api.user + 'likes');
  }
  getDisLikes(): Observable<OperationResult<any[]>> {
    return this.http.get<OperationResult<any[]>>(this.config.api.user + 'dislikes');
  }
  getSubscribes(): Observable<OperationResult<any[]>> {
    return this.http.get<OperationResult<any[]>>(this.config.api.user + 'subscribes');
  }
  getComments(): Observable<OperationResult<any[]>> {
    return this.http.get<OperationResult<any[]>>(this.config.api.user + 'comments');
  }
}
