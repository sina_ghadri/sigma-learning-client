
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OperationResult } from 'sigmasoft-ts';
import { ConfigurationService } from 'sigmasoft-ng';
import { Post } from '../models';

@Injectable()
export class PostService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }


  getUserPostById(id: number): Observable<OperationResult<any>> { return this.http.get<OperationResult<any>>(this.config.api.post + id + '/user'); }

  search(value: string): Observable<OperationResult<Post[]>> { return this.http.get<OperationResult<Post[]>>(this.config.api.post + `?search=${value}`); }

  bookmark(id: number): Observable<OperationResult<any>> { return this.http.post<OperationResult<any>>(this.config.api.post + id + '/bookmark', {}); }
  unbookmark(id: number): Observable<OperationResult<any>> { return this.http.delete<OperationResult<any>>(this.config.api.post + id + '/bookmark'); }

  like(id: number): Observable<OperationResult<any>> { return this.http.post<OperationResult<any>>(this.config.api.post + id + '/like', {}); }
  unlike(id: number): Observable<OperationResult<any>> { return this.http.delete<OperationResult<any>>(this.config.api.post + id + '/like'); }

  disLike(id: number): Observable<OperationResult<any>> { return this.http.post<OperationResult<any>>(this.config.api.post + id + '/disLike', {}); }
  undisLike(id: number): Observable<OperationResult<any>> { return this.http.delete<OperationResult<any>>(this.config.api.post + id + '/disLike'); }

  subscribe(id: number): Observable<OperationResult<any>> { return this.http.post<OperationResult<any>>(this.config.api.post + id + '/subscribe', {}); }
  unsubscribe(id: number): Observable<OperationResult<any>> { return this.http.delete<OperationResult<any>>(this.config.api.post + id + '/subscribe'); }

  getComments(id: number): Observable<OperationResult<any>> { return this.http.get<OperationResult<any>>(this.config.api.post + id + '/comments'); }
  insertComment(id: number,obj: any): Observable<OperationResult<any>> { return this.http.post<OperationResult<any>>(this.config.api.post + id + '/comment', obj); }
}
