
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OperationResult } from 'sigmasoft-ts';
import { ConfigurationService } from 'sigmasoft-ng';

@Injectable()
export class ContactUsService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  insertContactUs(obj: any): Observable<OperationResult<any>> {
    return this.http.post<OperationResult<any>>(this.config.api.contactUs, obj);
  }
}
