
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OperationResult } from 'sigmasoft-ts';
import { Lesson } from '../models';
import { ConfigurationService } from 'sigmasoft-ng';

@Injectable()
export class LessonService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getLessons(): Observable<OperationResult<Lesson[]>> {
    return this.http.get<OperationResult<Lesson[]>>(this.config.api.lesson);
  }
  getLessonById(id: number): Observable<OperationResult<Lesson>> {
    return this.http.get<OperationResult<Lesson>>(this.config.api.lesson + id);
  }
}
