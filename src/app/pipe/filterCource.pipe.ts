import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterCource',
  pure: false
})
export class FilterCourcePipe implements PipeTransform {

  transform(items: any[], key: number): any[] {

    if(!items) return [];
    if(!key) return items;
    if(key==4) return items;
    if(key==5) {return items.filter( it =>  it.type.key==0)};
    return items.filter( it =>  it.type.key==key)
  }
}
