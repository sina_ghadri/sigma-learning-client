import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterStatus',
  pure: false
})
export class FilterStatusPipe implements PipeTransform {

  transform(items: any[], key: number): any[] {

    if(!items) return [];
    if(!key) return items;
    if(key==4) return items;
    if(key==7) {return items.filter( it =>  it.status.key==0)};
    return items.filter( it =>  it.status.key==key)
  }
}
