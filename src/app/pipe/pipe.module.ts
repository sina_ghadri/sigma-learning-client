import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterStatusPipe } from './filterCourceByStatus.pipe';
import { FilterCourcePipe } from './filterCource.pipe';
import { SearchPipe } from './search.pipe';
import { JalaliPipe } from './jalali.pipe';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        FilterCourcePipe,
        FilterStatusPipe,
        JalaliPipe,
        SearchPipe
    ],
    exports: [
        FilterCourcePipe,
        FilterStatusPipe,
        JalaliPipe,
        SearchPipe
    ]
})
export class PipeModule { }
