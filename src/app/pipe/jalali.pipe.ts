import { Pipe, PipeTransform } from '@angular/core';
import { JDate } from 'sigmasoft-ts';

@Pipe({
  name: 'jalali',
  pure: false
})
export class JalaliPipe implements PipeTransform {

  transform(value: string, format: string = 'yyyy/MM/dd'): string {
    return JDate.toJDate(JDate.parseDate(value)).format(format);
  }
}
