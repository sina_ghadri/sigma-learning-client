import { Component, OnInit, Input } from '@angular/core';
import { AuthService, PostService } from '../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {
  @Input() id: number;
  @Input() parentId: number;

  get islogin(): boolean { return this.authService.islogin; }
  obj: any = {};
  message: string;
  color: string;
  progress: boolean;
  constructor(private authService: AuthService, private postService: PostService, private router: Router) { }

  ngOnInit() {
  }
  login() {
    this.authService.refrenceUrl = this.router.url;
    this.router.navigate(['login']);
  }
  submit() {
    this.progress = true;
    if(this.parentId) this.obj.parent = { id: this.parentId };
    this.postService.insertComment(this.id, this.obj).subscribe(op => {
      if (op.success) {
        this.message = 'دیدگاه شما با موفقیت ثبت شد';
        this.color = 'green';
        this.obj = {};
      }
      else {
        this.message = op.message ? op.message : 'خطا در ثبت دیدگاه رخ داد';
        this.color = 'red';
      }
      setTimeout(() => this.message = null, 4000);
    }, e => this.progress = false, () => this.progress = false);
  }
}
