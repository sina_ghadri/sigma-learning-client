import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PostService } from '../../services';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss']
})
export class CommentListComponent implements OnInit, OnChanges {
  @Input() id: number;
  @Input() repliable: boolean;

  items: any[];
  constructor(private postService: PostService) { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.postService.getComments(this.id).subscribe(op => this.items = op.success ? op.data : []);
  }
}
