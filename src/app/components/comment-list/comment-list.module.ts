import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentListComponent } from './comment-list.component';
import { CommentModule } from '../comment/comment.module';

@NgModule({
  imports: [
    CommonModule,

    CommentModule
  ],
  declarations: [CommentListComponent],
  exports: [CommentListComponent],
})
export class CommentListModule { }
