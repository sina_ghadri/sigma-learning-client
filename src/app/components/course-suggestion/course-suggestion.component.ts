import { Component, OnInit, Input } from '@angular/core';
import { CourseService } from '../../services';
import { Post } from '../../models';

@Component({
  selector: 'app-course-suggestion',
  templateUrl: './course-suggestion.component.html',
  styleUrls: ['./course-suggestion.component.scss']
})
export class CourseSuggestionComponent implements OnInit {

  @Input() count: number = 4;
  courses: any[] = [];
  get items() { return this.courses.filter((x,index) => index < this.count); }

  constructor(private service: CourseService) { }

  ngOnInit() {
    this.service.getCourses().subscribe(op => {
      this.courses = this.shuffle(op.data)
      setInterval(() => {
        this.courses = this.shuffle(this.courses)
      }, 20000)
    });
  }

  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }

}
