import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseSuggestionComponent } from './course-suggestion.component';
import { CourseCardModule } from '../course-card/course-card.module';

@NgModule({
  imports: [
    CommonModule,

    CourseCardModule,
  ],
  declarations: [CourseSuggestionComponent],
  exports: [CourseSuggestionComponent]
})
export class CourseSuggestionModule { }
