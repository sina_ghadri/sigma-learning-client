import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentComponent } from './comment.component';
import { CommentFormModule } from '../comment-form/comment-form.module';
import { PipeModule } from '../../pipe/pipe.module';

@NgModule({
  imports: [
    CommonModule,

    PipeModule,
    CommentFormModule
  ],
  declarations: [CommentComponent],
  exports: [CommentComponent],
})
export class CommentModule { }
