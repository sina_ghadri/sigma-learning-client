import { Component, OnInit, Input } from '@angular/core';
import { CommentService } from '../../services';
import { Modal } from 'sigmasoft-ng';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  @Input() comment: any;
  @Input() repliable: boolean;

  replies: any[];
  replying: boolean;

  get image() {
    return 'images/avatar.png';
  }

  constructor(private commentService: CommentService) { }

  ngOnInit() {
  }
  loadReplies() {
    this.commentService.getCommentByParentId(this.comment.id).subscribe(op => this.replies = op.success ? op.data : []);
  }
}
