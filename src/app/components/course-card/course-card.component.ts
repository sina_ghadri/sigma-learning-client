import { Component, OnInit, Input } from '@angular/core';
import { Course } from '../../models';
import { Formatter } from 'sigmasoft-ts';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})
export class CourseCardComponent implements OnInit {
  @Input() course: Course;

  constructor() { }

  ngOnInit() {
  }
  commaSep(value:string): string { return Formatter.seperate(value); }

}
