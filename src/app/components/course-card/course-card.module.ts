import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseCardComponent } from './course-card.component';
import { PostCardModule } from '../post-card/post-card.module';
import { PipeModule } from '../../pipe/pipe.module';

@NgModule({
  imports: [
    CommonModule,

    PipeModule,
    PostCardModule
  ],
  declarations: [CourseCardComponent],
  exports: [CourseCardComponent]

})
export class CourseCardModule { }
