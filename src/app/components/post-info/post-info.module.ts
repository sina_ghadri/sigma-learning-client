import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostInfoComponent } from './post-info.component';
import { PipeModule } from '../../pipe/pipe.module';

@NgModule({
  imports: [
    CommonModule,

    PipeModule,
  ],
  declarations: [PostInfoComponent],
  exports: [PostInfoComponent],
})
export class PostInfoModule { }
