import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Post, PostImage } from '../../models';
import { PostService, AuthService } from '../../services';

@Component({
  selector: 'app-post-info',
  templateUrl: './post-info.component.html',
  styleUrls: ['./post-info.component.scss']
})
export class PostInfoComponent implements OnInit, OnChanges {

  @Input() post: Post;
  
  userinfo: any = {};
  selectedImage: PostImage;

  get islogin(): boolean { return this.authService.islogin; }
  get href() { return location.href; }
  get image(): string {
    if (this.selectedImage) return this.selectedImage.url;
    if (this.post && this.post.images.length > 0) return this.post.images[0].url;
    return null;
  }

  constructor(private authService: AuthService, private postService: PostService) { }

  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.islogin) this.postService.getUserPostById(this.post.id).subscribe(op => this.userinfo = op.success ? op.data : {});
  }

  getRateClass(index) {
    let rateP = this.post.rateP | 0;
    let rateN = this.post.rateN | 0;
    let total = rateP + rateN;
    if (total == 0) return 'mdi-star-outline';
    let rate = Math.round((rateP * 4) / total);
    return index <= rate ? 'mdi-star' : 'mdi-star-outline';
  }


  bookmark() {
    this.userinfo.bookmark = (this.userinfo.bookmark ? false : true);
    if (this.userinfo.bookmark) this.postService.bookmark(this.post.id).subscribe();
    else this.postService.unbookmark(this.post.id).subscribe();
  }
  like() {
    this.userinfo.like = (this.userinfo.like ? false : true);
    if (this.userinfo.like) this.postService.like(this.post.id).subscribe();
    else this.postService.unlike(this.post.id).subscribe();
  }
  disLike() {
    this.userinfo.disLike = (this.userinfo.disLike ? false : true);
    if (this.userinfo.disLike) this.postService.disLike(this.post.id).subscribe();
    else this.postService.undisLike(this.post.id).subscribe();
  }
  subscribe() {
    this.userinfo.subscribe = (this.userinfo.subscribe ? false : true);
    if (this.userinfo.subscribe) this.postService.subscribe(this.post.id).subscribe();
    else this.postService.unsubscribe(this.post.id).subscribe();
  }
}
