import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Post, PostType } from '../../models';
import { Router } from '@angular/router';
import { LessonService } from '../../services/lesson.service';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {

  @Input() post: Post;

  get image(): string {
    if (this.post && this.post.images && this.post.images.length > 0 && this.post.images[0].url) return this.post.images[0].url;
    return 'images/404.png';
  }

  constructor(private router: Router, private lessonService: LessonService) { }

  ngOnInit() { }

  goto() {
    Object.assign(new Post, this.post).goto(this.router, this.lessonService);
  }
}
