import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleCardComponent } from './article-card.component';
import { PostCardModule } from '../post-card/post-card.module';
import { PipeModule } from '../../pipe/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    PipeModule,

    PostCardModule,
  ],
  declarations: [ArticleCardComponent],
  exports: [ArticleCardComponent]
})
export class ArticleCardModule { }
