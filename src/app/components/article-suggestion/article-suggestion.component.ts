import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ArticleService } from '../../services';
import { Post } from '../../models';

@Component({
  selector: 'app-article-suggestion',
  templateUrl: './article-suggestion.component.html',
  styleUrls: ['./article-suggestion.component.scss']
})
export class ArticleSuggestionComponent implements OnInit {

  @Input() count: number = 4;
  articles: any[];
  get items() { return this.articles.filter((x, index) => index < this.count); }
  constructor(private service: ArticleService) { }

  ngOnInit() {
    
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.service.getArticles().subscribe(op => this.articles = this.shuffle(op.data))
  }

  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
}
