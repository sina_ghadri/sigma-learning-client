import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleSuggestionComponent } from './article-suggestion.component';
import { ArticleCardModule } from '../article-card/article-card.module';

@NgModule({
  imports: [
    CommonModule,

    ArticleCardModule,
  ],
  declarations: [ArticleSuggestionComponent],
  exports: [ArticleSuggestionComponent]
})
export class ArticleSuggestionModule { }
