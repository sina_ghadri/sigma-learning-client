import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthService } from "./services";

@Injectable()
export class CanActivateUser implements CanActivate {
  constructor(private authService: AuthService) {}
 
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return this.authService.checklogin();
  }
}
 