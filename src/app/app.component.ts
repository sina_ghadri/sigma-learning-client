import { Component, OnInit } from '@angular/core';
import { Panel } from 'sigmasoft-ng/panel/admin/models';
import { HttpConfigService } from 'sigmasoft-ng/misc/jwt/http-config.service';
import { ConfigurationService } from 'sigmasoft-ng/misc';
import { Router, NavigationEnd } from '@angular/router';
import { AuthService } from './services';
import {fadeAnimation} from './animations/fadeAnimation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent implements OnInit {
  isload: boolean;
  toggleMenu: boolean = false;

  get islogin(): boolean { return this.authService.islogin; }

  public constructor(
    private httpConfig: HttpConfigService,
    private configService: ConfigurationService,
    private router: Router,
    public authService: AuthService
  ) { }
  ngOnInit(): void {
    this.httpConfig.on4xxUnAuthorize.subscribe(x => {
      this.router.navigate(['login']);
    });
    this.configService.loadConfiguration('config.json').subscribe(config => {
      this.isload = true;
      let token = this.authService.token;
      if (token) this.httpConfig.token = token;
      this.authService.checklogin().subscribe();
    });

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }

  logout() {
    this.authService.logout();
    var index = this.router.url.indexOf('user');
    if (index > -1 && index < 2) this.router.navigate(['']);
  }

  // onDeactivate() {
  //   // document.body.scrollTop = 0;
  //   // Alternatively, you can scroll to top by using this other call:
  //   window.scrollTo(0, 0)
  // }
}
