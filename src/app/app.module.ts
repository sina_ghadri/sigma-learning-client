import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { HttpService } from 'sigmasoft-ng/misc/jwt/http.service'
import { HttpConfigService } from "sigmasoft-ng/misc/jwt/http-config.service";
import { SigmaSoftModule } from 'sigmasoft-ng';

import { CourseService, ArticleService, AuthService, VipserviceService, LessonService,
  UserService, PostService, CommentService, ContactUsService, TransactionService } from "./services";
import { CanActivateUser } from "./app.guard";
import {  } from "./services/lesson.service";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SmoothScrollDirective, SmoothScrollToDirective} from './directives/smoothScroll.directive';
import {ScrollService} from './services/scroll.service';
import {AnimateOnScrollDirective} from './directives/animate-on-scroll.directive';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    SigmaSoftModule,
    BrowserAnimationsModule,
  ],
  exports: [
    AnimateOnScrollDirective,SmoothScrollToDirective, SmoothScrollDirective
  ],
  declarations: [AppComponent,SmoothScrollToDirective, SmoothScrollDirective,AnimateOnScrollDirective],
  providers: [
    HttpConfigService,
    CourseService,
    LessonService,
    PostService,
    TransactionService,
    ArticleService,
    VipserviceService,
    AuthService,
    UserService,
    ContactUsService,
    CommentService,
    CanActivateUser,

    ScrollService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
