import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CanActivateUser } from './app.guard';

const routes: Route[] = [
  { path: '', loadChildren: './pages/home/home.module#HomeModule', pathMatch: 'full' },
  { path: 'courses', loadChildren: './pages/courses/courses.module#CoursesModule' },
  { path: 'course/:id', loadChildren: './pages/course/course.module#CourseModule' },
  { path: 'transaction', loadChildren: './pages/transaction/transaction.module#TransactionModule' },
  { path: 'articles', loadChildren: './pages/articles/articles.module#ArticlesModule' },
  { path: 'article/:id', loadChildren: './pages/article/article.module#ArticleModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterModule' },
  { path: 'reset-password', loadChildren: './pages/reset-password/reset-password.module#ResetPasswordModule' },
  { path: 'about-us', loadChildren: './pages/about-us/about-us.module#AboutUsModule' },
  { path: 'contact-us', loadChildren: './pages/contact-us/contact-us.module#ContactUsModule' },
  { path: 'discussions', loadChildren: './pages/discussions/discussions.module#DiscussionsModule' },
  { path: 'discussion/:id', loadChildren: './pages/discussion/discussion.module#DiscussionModule' },
  { path: 'user', canActivate: [CanActivateUser], loadChildren: './pages/user/user.module#UserModule' },
  { path: '**', loadChildren: './pages/not-found/not-found.module#NotFoundModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
