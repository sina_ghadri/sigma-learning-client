import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { CoursesRoutingModule } from './course.routing';
import { CourseComponent } from './course.component';
import { PipeModule } from '../../pipe/pipe.module';

import { CourseSuggestionModule } from '../../components/course-suggestion/course-suggestion.module';
import { CommentFormModule } from '../../components/comment-form/comment-form.module';
import { PostInfoModule } from '../../components/post-info/post-info.module';
import { CommentListModule } from '../../components/comment-list/comment-list.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    CoursesRoutingModule,
    PipeModule,

    PostInfoModule,
    CourseSuggestionModule,
    CommentListModule,
    CommentFormModule,
  ],
  declarations: [CourseComponent]
})
export class CourseModule { }
