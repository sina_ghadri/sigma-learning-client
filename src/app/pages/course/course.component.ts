import { Component, OnInit, ElementRef, AfterContentInit } from '@angular/core';
import { CourseService } from '../../services/course.service';
import { Course, CourseType } from '../../models/course';
import { ActivatedRoute, Router } from '@angular/router';
import { Lesson } from '../../models/lesson';
import { AuthService } from '../../services';
import { TransactionService } from '../../services/transaction.service';
import { TransactionType } from '../../models';
import { Formatter } from 'sigmasoft-ts';

@Component({
  selector: 'page-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {
  obj: any = {};
  
  get course(): Course { return this.obj.course; }
  set course(value: Course) { this.obj.course = value; this.selected = null; this.lessons = []; }

  get lessons(): Lesson[] { return this.obj.lessons; }
  set lessons(value: Lesson[]) { this.obj.lessons = value; }

  courseId: number;
  lessonId: number;
  selected: Lesson;
  tab: string = 'course';
  progress: boolean;

  get isbuy(): boolean { return this.obj.isBuy; }
  get isvip(): boolean { return this.obj.isVip; }
  get islogin(): boolean { return this.authService.islogin; }

  get image(): string {
    if (this.selected && this.selected.post && this.selected.post.images && this.selected.post.images.length > 0 && this.selected.post.images[0].url) return this.selected.post.images[0].url;
    if (this.course && this.course.post && this.course.post.images && this.course.post.images.length > 0 && this.course.post.images[0].url) return this.course.post.images[0].url;
    return 'images/404.png';
  }
  get video(): string {
    if (!this.islogin) return null;
    if (this.course && this.selected && !this.selected.isFree) {
      switch (this.course.type.key) {
        case CourseType.Free: return null;
        case CourseType.Vip: if (this.isvip) return null; break;
        case CourseType.OnSale: if (this.isbuy) return null; break;
      }
    }
    return this.selected.url;
  }

  constructor(
    private courseService: CourseService,
    private authService: AuthService,
    private transactionService: TransactionService,
    private router: Router,
    private route: ActivatedRoute,
    private element: ElementRef) { }

  ngOnInit() {
    this.course = null;
    this.selected = null;
    this.lessons = [];
    this.route.queryParams.subscribe(params => {
      this.lessonId = +params['lesson'];
    });
    this.route.params.subscribe(params => {
      this.courseId = +params['id'];
      this.loadCourse();
    });
  }

  islock(lesson: Lesson): boolean {
    if (!this.islogin) return true;
    if (!lesson.isFree) {
      switch (this.course.type.key) {
        case CourseType.Free: return false;
        case CourseType.Vip: if (this.isvip) return false; break;
        case CourseType.OnSale: if (this.isbuy) return false; break;
      }
    }
    return (lesson.url ? false : true);
  }
  commaSep(value:string): string { return Formatter.seperate(value); }

  loadCourse() {
    this.courseService.getCourseById(this.courseId).subscribe(op => {
      if (op.success) {
        this.obj = op.data;
        this.loadLesson();
      } else {
        this.router.navigate(['']);
      }
    });
    this.tab = 'course';
  }
  loadLesson() {
    if (this.lessonId) this.selected = this.lessons.find(x => x.id == this.lessonId);
    if (!this.selected && this.lessons.length > 0) this.selected = this.lessons[0];
  }
  courseTime(): string {
    let sum = 0;
    for (let i = 0; i < this.lessons.length; i++) sum += this.lessons[i].time;
    return this.timeFormatter(sum);
  }
  timeFormatter(time): string {
    let hour = Math.floor(time / 3600);
    let min = Math.floor((time % 3600) / 60);
    let sec = Math.floor(time % 60);
    return (hour > 0 ? (hour < 10 ? '0' : '') + hour + ':' : '') + (min > 0 ? (min < 10 ? '0' : '') + min + ':' : '') + (sec < 10 ? '0' : '') + sec;
  }
  changeLesson(lesson) {
    this.selected = lesson;

    this.router.navigate([], {
      queryParams: { lesson: this.selected.id },
      relativeTo: this.route
    });

    let video = this.element.nativeElement.querySelector('video');
    if (video) {
      let source = video.querySelector('source');
      video.pause();
      source.setAttribute('src', lesson.url);
      video.load();
    }
  }

  login() {
    this.authService.refrenceUrl = this.router.url;
    this.router.navigate(['login']);
  }
  vip() {
    this.router.navigate(['user', 'vipservice']);
  }
  buy() {
    this.progress = true;
    this.transactionService.pay({ referenceId: this.course.id, type: TransactionType.Course }).subscribe(op => {
      if (op.success) location.href = op.data;
    }, e => this.progress = false, () => this.progress = false);
  }
}
