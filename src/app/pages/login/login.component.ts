import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { KeyValue } from 'sigmasoft-ts';
import { AuthService } from '../../services';
import { CaptchaComponent } from 'sigmasoft-ng/misc/captcha/components/captcha.component';
import { HttpConfigService, ConfigurationService } from 'sigmasoft-ng';
import { Router } from '@angular/router';

@Component({
  selector: 'page-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild(CaptchaComponent) captcha: CaptchaComponent;
  obj: any = {
    captcha: new KeyValue
  };
  progress: boolean;
  message: string;
  usernameElement: HTMLInputElement;
  captchaElement: HTMLInputElement;
  get captchaUrl(): string { return this.configService.configuration.captchaUrl; }
  constructor(
    private configService: ConfigurationService,
    private httpConfig: HttpConfigService, 
    private router: Router, 
    private service: AuthService, 
    private element: ElementRef
    ) { }

  ngOnInit() {
    this.usernameElement = this.element.nativeElement.querySelector('[name="username"]');
    this.captchaElement = this.element.nativeElement.querySelector('[name="captchaValue"]');

    this.usernameElement.focus();
  }

  submit() {
    this.progress = true;
    this.service.login(this.obj).subscribe(op => {
      if (op.success) {
        this.httpConfig.token = op.data;
        localStorage.setItem('token', op.data);
        if(this.service.refrenceUrl) {
          this.router.navigateByUrl(this.service.refrenceUrl);
          this.service.refrenceUrl = null;
        } else this.router.navigate(['user']);
      }
      else {
        this.captcha.refresh();
        this.obj.captcha.value = '';

        if (op.message) this.message = op.message;
        else this.message = 'خطایی در ورود رخ داده است لطفا در قسمت ارتباط با ما به مدیر سایت اطلاع دهید';

        setTimeout(() => this.message = null, 4000);

        if(op.code == 2)
        {
          this.obj.username = '';
          this.obj.password = '';
          this.usernameElement.focus();
        }
        else {
          this.captchaElement.focus();
        }
      }
    }, e => this.progress = false, () => this.progress = false);
  }
}
