import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TransactionService } from '../../services';
import { TransactionType } from '../../models';

@Component({
  selector: 'page-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  constructor(
    private service: TransactionService, 
    private router: Router,
    private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      let authority = params['Authority'];
      let isok = params['Status'] == 'OK';

      if (authority) {
        this.service.veify({ authority: authority, isOk: isok }).subscribe(op => {
          if(op.success)
          {
            if(op.data.type == TransactionType.Course)
            {
              this.router.navigate(['course', op.data.referenceId]);
            }
            else if(op.data.type == TransactionType.VipService)
            {
              this.router.navigate(['user','vipservice']);
            }
          }
        })
      }
    })

  }
}
