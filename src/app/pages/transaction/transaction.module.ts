import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { TransactionRoutingModule } from './transaction.routing';
import { TransactionComponent } from './transaction.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    TransactionRoutingModule
  ],
  declarations: [TransactionComponent]
})
export class TransactionModule { }
