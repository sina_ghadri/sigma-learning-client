import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { ContactUsRoutingModule } from './contact-us.routing';
import { ContactUsComponent } from './contact-us.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    ContactUsRoutingModule
  ],
  declarations: [ContactUsComponent]
})
export class ContactUsModule { }
