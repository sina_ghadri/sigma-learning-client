import { Component, OnInit, ViewChild } from '@angular/core';
import { CaptchaComponent } from 'sigmasoft-ng/misc/captcha/components/captcha.component';
import { KeyValue } from 'sigmasoft-ts';
import { ContactUsService } from '../../services';
import { ConfigurationService } from 'sigmasoft-ng';

@Component({
  selector: 'page-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  obj: any = {
    contactus: {},
    captcha: new KeyValue
  };
  titles: string[] = [
    'درخواست تدریس دوره',
    'ثبت مشکل در سیستم',
    'سایر موارد'
  ]
  @ViewChild(CaptchaComponent) captcha: CaptchaComponent;
  message: string;
  color: string;
  progress: boolean;
  get captchaUrl(): string { return this.configService.configuration.captchaUrl; }
  constructor(
    private configService: ConfigurationService,
    private service: ContactUsService, 
    ) { }

  ngOnInit() {
  }
  submit() {
    this.progress = true;
    this.service.insertContactUs(this.obj).subscribe(op => {
      if (op.success) {
        this.message = 'ثبت با موفقیت انجام شد';
        this.color = 'green';
        this.obj = null;
      } else {
        this.message = op.message ? op.message : 'ثبت با مشکل مواجه شد';
        this.color = 'red';
        this.obj.captcha.value = '';
        this.captcha.refresh();
      }
      setTimeout(() => this.message = null, 4000);
    }, e => this.progress = false, () => this.progress = false)
  }

}
