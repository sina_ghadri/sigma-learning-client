import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { CoursesRoutingModule } from './courses.routing';
import { CoursesComponent } from './courses.component';
import { PipeModule } from '../../pipe/pipe.module';
import { CourseCardModule } from '../../components/course-card/course-card.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    CoursesRoutingModule,
    PipeModule,

    CourseCardModule
  ],
  declarations: [CoursesComponent]
})
export class CoursesModule { }
