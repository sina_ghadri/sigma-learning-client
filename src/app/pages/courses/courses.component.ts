import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../services/course.service';
import { Course } from '../../models/course';

@Component({
  selector: 'page-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  sort: string = 'publishTime';
  search: string;
  type: number = 4;
  status: number = 4;
  courses: Course[] = [];
  constructor(private courseService: CourseService) { }

  ngOnInit() {
    this.courseService.getCourses().subscribe(data => { this.courses = data.data });
  }
  sortFilter(items, func: string): any[] {
    let funcs = {
      publishTime: (x: Course) => x.post.publishTime,
      price: (x: Course) => x.price
    };
    return items.sort((x1, x2) => {
      let f = funcs[func];
      x1 = f(x1);
      x2 = f(x2);
      if (x1 > x2) return 1;
      if (x2 > x1) return -1;
      return 0;
    })
  }
  searchFilter(items: any[], search: string): any[] {
    if (!search) return items;
    return items.filter((x: Course) => {
      if (x.post.title.indexOf((search)) > -1) return true;
      if (x.post.description.indexOf((search)) > -1) return true;
      if (x.post.body.indexOf((search)) > -1) return true;
      return false;
    })
  }
  getItems(): any[] {
    let items = this.courses;
    items = this.sortFilter(items, this.sort);
    items = this.searchFilter(items, this.search);
    return items;
  }
}
