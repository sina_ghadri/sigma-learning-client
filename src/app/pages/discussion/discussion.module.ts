import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { DiscussionRoutingModule } from './discussion.routing';
import { DiscussionComponent } from './discussion.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    DiscussionRoutingModule
  ],
  declarations: [DiscussionComponent]
})
export class DiscussionModule { }
