import { Component, OnInit, ViewChild } from '@angular/core';
import { KeyValue } from 'sigmasoft-ts';
import { AuthService } from '../../services';
import { Router } from '@angular/router';
import { CaptchaComponent } from 'sigmasoft-ng/misc/captcha/components/captcha.component';
import { ConfigurationService } from 'sigmasoft-ng';

@Component({
  selector: 'page-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild(CaptchaComponent) captcha: CaptchaComponent;
  obj: any = {
    captcha: new KeyValue
  };
  message: string;
  progress: boolean;
  get captchaUrl(): string { return this.configService.configuration.captchaUrl; }
  constructor(
    private configService: ConfigurationService,
    private service: AuthService, 
    private router: Router
    ) { }

  ngOnInit() {
  }
  submit() {
    this.progress = true;
    this.service.register(this.obj).subscribe(op => {
      if (op.success) {
        this.service.token = op.data;
        this.router.navigate(['user']);
      }
      else {
        this.captcha.refresh();
        this.obj.captcha.value = '';
        this.message = op.message ? op.message : 'ثبت با مشکل مواجه شد';
      }
    }, e => this.progress = false, () => this.progress = false)
  }
}
