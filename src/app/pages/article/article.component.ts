import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from '../../models';
import { Location } from '@angular/common';

@Component({
  selector: 'page-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  article: any;

  constructor(
    private service: ArticleService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.service.getArticleById(+params['id']).subscribe(op => {
        if (op.success) {
          this.article = op.data;
        }
        else {
          this.location.back();
        }
      });
    });
  }
}
