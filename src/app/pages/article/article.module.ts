import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { ArticleRoutingModule } from './article.routing';
import { ArticleComponent } from './article.component';
import { CommentFormModule } from '../../components/comment-form/comment-form.module';
import { PostInfoModule } from '../../components/post-info/post-info.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    ArticleRoutingModule,

    PostInfoModule,
    CommentFormModule
  ],
  declarations: [ArticleComponent]
})
export class ArticleModule { }
