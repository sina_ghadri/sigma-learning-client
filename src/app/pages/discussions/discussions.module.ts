import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { DiscussionsRoutingModule } from './discussions.routing';
import { DiscussionsComponent } from './discussions.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    DiscussionsRoutingModule
  ],
  declarations: [DiscussionsComponent]
})
export class DiscussionsModule { }
