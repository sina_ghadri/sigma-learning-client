import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DiscussionsComponent } from './discussions.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: DiscussionsComponent }])],
  exports: [RouterModule]
})
export class DiscussionsRoutingModule { }
