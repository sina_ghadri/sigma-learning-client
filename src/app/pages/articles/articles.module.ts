import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { ArticlesRoutingModule } from './articles.routing';
import { ArticlesComponent } from './articles.component';
import { ArticleCardModule } from '../../components/article-card/article-card.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    ArticlesRoutingModule,

    ArticleCardModule,
  ],
  declarations: [ArticlesComponent]
})
export class ArticlesModule { }
