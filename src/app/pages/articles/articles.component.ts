import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services';

@Component({
  selector: 'page-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {

  articles: any[] = [];
  search: string;

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.articleService.getArticles().subscribe(op => this.articles = op.success ? op.data : []);
  }
  searchFilter(items: any[], search: string): any[] {
    if (!search) return items;
    return items.filter((x: any) => {
      if (x.post.title.indexOf((search)) > -1) return true;
      if (x.post.description.indexOf((search)) > -1) return true;
      if (x.post.body.indexOf((search)) > -1) return true;
      return false;
    })
  }
  getItems(): any[] {
    let items = this.articles;
    items = this.searchFilter(items, this.search);
    return items;
  }
}
