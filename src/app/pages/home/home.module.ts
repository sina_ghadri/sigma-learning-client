import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home.routing';
import { HomeComponent } from './home.component';
import { PipeModule } from '../../pipe/pipe.module';

import { CourseCardModule } from '../../components/course-card/course-card.module';
import { ArticleCardModule } from '../../components/article-card/article-card.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    HomeRoutingModule,
    PipeModule,

    CourseCardModule,
    ArticleCardModule,
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
