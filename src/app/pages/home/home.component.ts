import { Component, OnInit, ElementRef } from '@angular/core';
import { CourseService } from '../../services/course.service';
import { Course } from '../../models/course';
import { ArticleService } from '../../services/article.service';
import { PostService } from '../../services';
import { Router } from '@angular/router';
import { Post, PostType } from '../../models';
import { LessonService } from '../../services/lesson.service';

@Component({
  selector: 'page-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  _searchValue: string;

  courses: Course[] = [];
  articles: any[] = [];
  get searchValue(): string { return this._searchValue; }
  set searchValue(value: string) { this._searchValue = value; this.searching(); }
  searchTimer: any;
  searchResults: any[];
  searchElement: HTMLInputElement;
  constructor(
    private element: ElementRef,
    private postService: PostService,
    private lessonService: LessonService,
    private courseService: CourseService,
    private articleService: ArticleService,
    private router: Router
  ) { }

  ngOnInit() {
    this.courseService.getCourses().subscribe(op => this.courses = op.success ? op.data.slice((op.data.length > 4 ? op.data.length - 4 : 0), op.data.length) : []);
    this.articleService.getArticles().subscribe(op => this.articles = op.success ? op.data.slice((op.data.length > 4 ? op.data.length - 4 : 0), op.data.length) : []);

    this.searchElement = this.element.nativeElement.querySelector('.search-form input');
    this.searchElement.focus();
  }

  searching() {
    this.searchResults = [];
    if (this.searchValue) {
      if (this.searchTimer) clearTimeout(this.searchTimer);
      this.searchTimer = setTimeout(() => {
        this.postService.search(this.searchValue).subscribe(op => this.searchResults = op.success ? op.data : [])
      }, 1000);
    }
  }

  gotoPost(post: Post) {
    Object.assign(new Post,post).goto(this.router, this.lessonService);
  }
}
