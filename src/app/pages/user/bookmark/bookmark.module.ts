import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { BookmarkRoutingModule } from './bookmark.routing';
import { BookmarkComponent } from './bookmark.component';
import { PostCardModule } from '../../../components/post-card/post-card.module';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    BookmarkRoutingModule,

    PostCardModule
  ],
  declarations: [BookmarkComponent]
})
export class BookmarkModule { }
