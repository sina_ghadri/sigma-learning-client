import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BookmarkComponent } from './bookmark.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: BookmarkComponent }])],
  exports: [RouterModule]
})
export class BookmarkRoutingModule { }
