import { Component, OnInit } from '@angular/core';
import { UserService, PostService } from '../../../services';

@Component({
  selector: 'page-user-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.scss']
})
export class LikeComponent implements OnInit {

  progress: boolean;
  items: any[];
  constructor(private service: UserService, private postService: PostService) { }

  ngOnInit() {
    this.load();
  }
  load() {
    this.progress = true;
    this.service.getLikes().subscribe(op => this.items = op.success ? op.data : [], e => this.progress = false, () => this.progress = false)
  }
  delete(item: any) {
    this.postService.unlike(item.post.id).subscribe(op => {
      if(op.success) this.items.splice(this.items.indexOf(item),1);
    });
  }
}
