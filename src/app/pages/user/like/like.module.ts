import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { LikeRoutingModule } from './like.routing';
import { LikeComponent } from './like.component';
import { PostCardModule } from '../../../components/post-card/post-card.module';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    LikeRoutingModule,

    PostCardModule
  ],
  declarations: [LikeComponent]
})
export class LikeModule { }
