import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LikeComponent } from './like.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: LikeComponent }])],
  exports: [RouterModule]
})
export class LikeRoutingModule { }
