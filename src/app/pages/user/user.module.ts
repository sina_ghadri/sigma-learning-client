import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { UserRoutingModule } from './user.routing';
import { UserComponent } from './user.component';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    UserRoutingModule
  ],
  declarations: [UserComponent]
})
export class UserModule { }
