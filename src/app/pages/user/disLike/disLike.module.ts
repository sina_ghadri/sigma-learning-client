import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { DisLikeRoutingModule } from './dislike.routing';
import { DisLikeComponent } from './disLike.component';
import { PostCardModule } from '../../../components/post-card/post-card.module';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    DisLikeRoutingModule,

    PostCardModule
  ],
  declarations: [DisLikeComponent]
})
export class DisLikeModule { }
