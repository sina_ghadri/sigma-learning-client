import { Component, OnInit } from '@angular/core';
import { UserService, PostService } from '../../../services';

@Component({
  selector: 'page-user-disLike',
  templateUrl: './disLike.component.html',
  styleUrls: ['./disLike.component.scss']
})
export class DisLikeComponent implements OnInit {

  progress: boolean;
  items: any[];
  constructor(private service: UserService, private postService: PostService) { }

  ngOnInit() {
    this.load();
  }
  load() {
    this.progress = true;
    this.service.getDisLikes().subscribe(op => this.items = op.success ? op.data : [], e => this.progress = false, () => this.progress = false)
  }
  delete(item: any) {
    this.postService.undisLike(item.post.id).subscribe(op => {
      if(op.success) this.items.splice(this.items.indexOf(item),1);
    });
  }
}
