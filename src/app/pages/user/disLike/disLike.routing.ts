import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DisLikeComponent } from './disLike.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: DisLikeComponent }])],
  exports: [RouterModule]
})
export class DisLikeRoutingModule { }
