import { Component, OnInit } from '@angular/core';
import { UserService, PostService } from '../../../services';

@Component({
  selector: 'page-user-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

  progress: boolean;
  items: any[];
  constructor(private service: UserService, private postService: PostService) { }

  ngOnInit() {
    this.load();
  }
  load() {
    this.progress = true;
    this.service.getSubscribes().subscribe(op => this.items = op.success ? op.data : [], e => this.progress = false, () => this.progress = false)
  }
  delete(item: any) {
    this.postService.unsubscribe(item.post.id).subscribe(op => {
      if(op.success) this.items.splice(this.items.indexOf(item),1);
    });
  }
}
