import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SubscribeComponent } from './subscribe.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: SubscribeComponent }])],
  exports: [RouterModule]
})
export class SubscribeRoutingModule { }
