import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { SubscribeRoutingModule } from './subscribe.routing';
import { SubscribeComponent } from './subscribe.component';
import { PostCardModule } from '../../../components/post-card/post-card.module';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    SubscribeRoutingModule,

    PostCardModule
  ],
  declarations: [SubscribeComponent]
})
export class SubscribeModule { }
