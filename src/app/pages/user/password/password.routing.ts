import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PasswordComponent } from './password.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: PasswordComponent }])],
  exports: [RouterModule]
})
export class PasswordRoutingModule { }
