import { Component, OnInit, ElementRef } from '@angular/core';
import { UserService } from '../../../services';

@Component({
  selector: 'page-user-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {

  obj: any = {};
  message: string;
  color: string;
  progress: boolean;
  oldPasswordElement: HTMLInputElement;
  constructor(private service: UserService, private element: ElementRef) { }

  ngOnInit() {
    this.oldPasswordElement = this.element.nativeElement.querySelector('[name="oldPassword"]')
  }
  submit() {
    this.progress = true;
    this.service.changePassword(this.obj).subscribe(op => {
      if (op.success) {
        this.message = 'کلمه عبور با موفقیت تغییر یافت';
        this.color = 'green';
      }
      else {
        this.message = (op.message ? op.message : 'خطا در ثبت اطلاعات');
        this.color = 'red';
      }
      setTimeout(() => this.message = null, 4000);
      this.obj = {};
    }, e => this.progress = false, () => this.progress = false)
  }
}
