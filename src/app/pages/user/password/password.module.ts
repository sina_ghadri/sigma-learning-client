import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { PasswordRoutingModule } from './password.routing';
import { PasswordComponent } from './password.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    PasswordRoutingModule
  ],
  declarations: [PasswordComponent]
})
export class PasswordModule { }
