import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services';

@Component({
  selector: 'page-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  obj: any = {};
  message: string;
  color: string;
  progress: boolean;
  constructor(private service: UserService) { }

  ngOnInit() {
    this.load()
  }
  load() {
    this.progress = true;
    this.service.getProfile().subscribe(op => this.obj = op.success ? op.data : {}, e => this.progress = false, () => this.progress = false);
  }
  submit() {
    this.progress = true;
    this.service.updateProfile(this.obj).subscribe(op => {
      if(op.success) {
        this.obj = op.data;
        this.message = 'ثبت اطلاعات با موفقیت انجام شد';
        this.color = 'green';
      } else {
        this.message = op.message ? op.message : 'ثبت با مشکل مواجه شد';
        this.color = 'red';
      }
      setTimeout(() => this.message = null, 4000);
    }, e => this.progress = false, () => this.progress = false);
  }
}
