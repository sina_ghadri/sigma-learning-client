import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { CourseRoutingModule } from './course.routing';
import { CourseComponent } from './course.component';
import { CourseCardModule } from '../../../components/course-card/course-card.module';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    CourseRoutingModule,

    CourseCardModule,
  ],
  declarations: [CourseComponent]
})
export class CourseModule { }
