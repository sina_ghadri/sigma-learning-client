import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services';

@Component({
  selector: 'page-user-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {

  progress: boolean;
  items: any[];
  constructor(private service: UserService) { }

  ngOnInit() {
    this.load();
  }
  load() {
    this.progress = true;
    this.service.getCourses().subscribe(op => this.items = op.success ? op.data : [], e => this.progress = false, () => this.progress = false);
  }
}
