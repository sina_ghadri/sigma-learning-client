import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { CommentRoutingModule } from './comment.routing';
import { CommentComponent } from './comment.component';

import { CommentModule as AppCommentModule } from '../../../components/comment/comment.module'

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    CommentRoutingModule,

    AppCommentModule,
  ],
  declarations: [CommentComponent]
})
export class CommentModule { }
