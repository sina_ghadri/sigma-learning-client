import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services';
import { Post } from '../../../models';
import { Router } from '@angular/router';
import { LessonService } from '../../../services/lesson.service';

@Component({
  selector: 'page-user-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  progress: boolean;
  items: any[];
  constructor(private service: UserService, private router: Router, private lessonService: LessonService) { }

  ngOnInit() {
    this.load();
  }
  load() {
    this.progress = true;
    this.service.getComments().subscribe(op => {
      if (op.success) {
        let items = [];
        for (let i = 0; i < op.data.length; i++) {
          let item = op.data[i];
          if (items.findIndex(x => x.post.id == item.post.id) < 0) 
            items.push({ 
              show: false,
              post: Object.assign(new Post, item.post), 
              comments: op.data.filter(x => x.post.id == item.post.id) 
            });
        }
        this.items = items;
      }
    }, e => this.progress = false, () => this.progress = false);
  }
  gotoPost(post: Post) {
    post.goto(this.router, this.lessonService);
  }
}
