import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services';
import { TransactionStatus } from '../../../models';

@Component({
  selector: 'page-user-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  progress: boolean;
  items: any[] = [];
  constructor(private service: UserService) { }
  ngOnInit() {
    this.load();
  }
  load() {
    this.progress = true;
    this.service.getTransactions().subscribe(op => this.items = op.success ? op.data : [], e => this.progress = false, () => this.progress = false)
  }
  getColor(item: any)
  {
    switch(item.status.key)
    {
      case TransactionStatus.Draft: return 'gray-light';
      case TransactionStatus.SendToPortal: return 'sky-dark';
      case TransactionStatus.Verifying: return 'orange';
      case TransactionStatus.Verified: return 'violet-light';
      case TransactionStatus.PaidSuccessfull: return 'green-dark';
      case TransactionStatus.Cancel: return 'red';
    }
  }
}
