import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { TransactionRoutingModule } from './transaction.routing';
import { TransactionComponent } from './transaction.component';
import { PipeModule } from '../../../pipe/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    TransactionRoutingModule,
    PipeModule
  ],
  declarations: [TransactionComponent]
})
export class TransactionModule { }
