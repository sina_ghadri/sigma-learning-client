import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VipserviceComponent } from './vipservice.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: VipserviceComponent }])],
  exports: [RouterModule]
})
export class VipserviceRoutingModule { }
