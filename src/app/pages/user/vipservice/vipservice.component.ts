import { Component, OnInit } from '@angular/core';
import { VipserviceService } from '../../../services/vipservice.service';
import { UserService, TransactionService } from '../../../services';
import { TransactionType } from '../../../models';

@Component({
  selector: 'page-user-vipservice',
  templateUrl: './vipservice.component.html',
  styleUrls: ['./vipservice.component.scss']
})
export class VipserviceComponent implements OnInit {

  obj: any = {};
  progress: boolean;
  services: any[] = [];
  items: any[] = [];
  remainDays: number;
  get status() {
    if(this.items.length > 0) {
      if (this.remainDays > 10) return 3;
      if (this.remainDays > 0) return 2;
      return 1;
    }
    return 0;
  }

  constructor(
    private service: UserService, 
    private transactionService: TransactionService,
    private vipserviceService: VipserviceService) { }

  ngOnInit() {
    this.progress = true;
    this.vipserviceService.getVipservices().subscribe(op => {
      this.services = op.success ? op.data : [];
      if(this.services.length > 0) this.obj.service = this.services[0];
    }, e => this.progress = false, () => this.progress = false);
    this.load();
  }
  load() {
    this.progress = true;
    this.service.getVipServices().subscribe(op => {
      if(op.success)
      {
        this.remainDays = op.data.remainDays;
        this.items = op.data.items;
      }
    }, e => this.progress = false, () => this.progress = false);
  }
  buy() {
    this.progress = true;
    this.transactionService.pay({ referenceId: this.obj.service.id, type: TransactionType.VipService }).subscribe(op => {
      if(op.success) location.href = op.data;
    }, e => this.progress = false, () => this.progress = false)
  }
}
