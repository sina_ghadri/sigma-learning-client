import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { VipserviceRoutingModule } from './vipservice.routing';
import { VipserviceComponent } from './vipservice.component';
import { PipeModule } from '../../../pipe/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    VipserviceRoutingModule,

    PipeModule
  ],
  declarations: [VipserviceComponent]
})
export class VipserviceModule { }
