import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserComponent } from './user.component';

@NgModule({
  imports: [RouterModule.forChild([{
    path: '', component: UserComponent, children: [
      { path: '', loadChildren: './profile/profile.module#ProfileModule' },
      { path: 'password', loadChildren: './password/password.module#PasswordModule' },
      { path: 'comment', loadChildren: './comment/comment.module#CommentModule' },
      { path: 'course', loadChildren: './course/course.module#CourseModule' },
      { path: 'bookmark', loadChildren: './bookmark/bookmark.module#BookmarkModule' },
      { path: 'like', loadChildren: './like/like.module#LikeModule' },
      { path: 'disLike', loadChildren: './disLike/disLike.module#DisLikeModule' },
      { path: 'subscribe', loadChildren: './subscribe/subscribe.module#SubscribeModule' },
      { path: 'transaction', loadChildren: './transaction/transaction.module#TransactionModule' },
      { path: 'vipservice', loadChildren: './vipservice/vipservice.module#VipserviceModule' },
    ]
  }])],
  exports: [RouterModule]
})
export class UserRoutingModule { }
