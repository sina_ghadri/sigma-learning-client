export class Global {
  static compareWithKey(val1: any, val2: any): boolean { return (!val1 && !val2) || (val1 && val2 && val1.key == val2.key); }
  static compareWithId(val1: any, val2: any): boolean { return (!val1 && !val2) || (val1 && val2 && val1.id == val2.id); }
  static getQueryStringValue(name: any, url?: any): any {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
}