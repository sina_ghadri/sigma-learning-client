export class OperationResult<T> {
    code: number;
    success: boolean = true;
    message: string;
    data: T;
}