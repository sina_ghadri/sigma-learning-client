export class Formatter {
    public static seperate(value: any, count: number = 3, char: string = ','): string {
        let tmp = '';
        count = count ? count : 3;
        char = char ? char : ',';
        if (value != null && value != undefined) {
            value = value.toString();
            for (let j = 0, i = value.length - 1; i >= 0; i-- , j++) 
                tmp = value[i] + (j > 0 && j % count == 0 ? char : '') + tmp;
        }
        return tmp;
    }
}