import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

export class Saver {
    public static file(content,name:string = 'filename', mimeType:string = 'text/plain') {
        var file = new File([content], name, {type: mimeType});
        saveAs(file);
        // let fileSaver: any = new FileSaver();
        // fileSaver.responseData = content;
        // fileSaver.strFileName = name;
        // fileSaver.strMimeType = mimeType;
        // fileSaver.initSaveFile();
    }
    public static string2ArrayBuffer(str:string) {
        const buf = new ArrayBuffer(str.length);
        const view = new Uint8Array(buf);
        for (let i = 0; i !== str.length; ++i) {view[i] = str.charCodeAt(i) & 0xFF;};
        return buf;
    }
    public static xlsx(data,name:string) {
        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        const wbout: string = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
        Saver.file(this.string2ArrayBuffer(wbout),name,'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }
}