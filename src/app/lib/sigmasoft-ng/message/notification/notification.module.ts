import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { NotificationComponent } from "./notification.component";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [NotificationComponent],
  exports: [NotificationComponent]
})
export class MessageNotificationModule {}
