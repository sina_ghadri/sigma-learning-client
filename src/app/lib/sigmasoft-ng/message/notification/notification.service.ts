import { Injectable } from "@angular/core";
import { NotificationItem } from "./models";

@Injectable({providedIn: 'root'})
export class NotificationService {
    private _items: NotificationItem[] = [];

    get items() : NotificationItem[] { return this._items.filter(val => true); }

    addItem(item:NotificationItem): NotificationService {
        this._items.push(item);
        setTimeout(() => item.show(),100);
        if(item.timeout) setTimeout(() => this.removeItem(item),item.timeout);
        return this;
    }
    removeItem(item: NotificationItem): NotificationService {
        item.hide();
        setTimeout(() => this._items.splice(this._items.indexOf(item),1),500);
        return this;
    }
}