import { Directive, EventEmitter, ElementRef, Input, OnChanges } from "@angular/core";

@Directive({
    selector: '[ss-input-focus]'
  })
  export class FocusDirective implements OnChanges {
    @Input() focus:boolean;
   
    constructor(private element: ElementRef) {}

    ngOnChanges(): void {
        this.element.nativeElement.focus()
    }
}