import { JDate, DateTime } from "../../../../sigmasoft-ts";
import { IGlobalization } from "../interfaces";

export enum DatePickerType { Gregorian = 1, Jalali = 2 }

export class DatePickerOptions {
    private _type: DatePickerType;
    private _globalization: IGlobalization;

    language: string;

    get type(): DatePickerType { return this._type; }
    set type(value: DatePickerType) {
        this._type = value;
        if(value == DatePickerType.Gregorian) {
            this._globalization = new GregorianGlobalization();
            this.language = "en";
        } else {
            this._globalization = new JalaliGlobalization();
            this.language = "fa";
        }
     }
    get globalization(): IGlobalization { return this._globalization; }

    constructor(type: DatePickerType = DatePickerType.Gregorian) { this.type = type; }

    getMonthesName(): string[] { return this.globalization.getMonthesName(this.language); }
    getWeekDaysName(): string[] { return this.globalization.getWeekDaysName(this.language); }
    getMonthDaysCount(): number[] { return this.globalization.getMonthDaysCount(); }
}
class JalaliGlobalization implements IGlobalization {
    private _montheNames: any = {
        fa: ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"]
    }
    private _weekDayNames: any = {
        fa: ["ش", "ی", "د", "س", "چ", "پ", "ج"]
    }
    private _monthDaysCount: number[] = [0, 31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29];
    getMonthesName(language: string): string[] { return this._montheNames[language]; }
    getWeekDaysName(language: string): string[] { return this._weekDayNames[language]; }
    getMonthDaysCount(): number[] { return this._monthDaysCount; }
    getStartDayOfWeek(year: number,month: number): number { return new JDate(year,month,1).toDate().getDay() + 2; }
    getDayOfWeek(date: DateTime) { return date.toJDate().getDayOfWeek(); }
    today() { return JDate.now; }
}
class GregorianGlobalization implements IGlobalization {
    private _montheNames: any = {
        en: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    }
    private _weekDayNames: any = {
        en: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    }
    private _monthDaysCount: number[] = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    getMonthesName(language: string): string[] { return this._montheNames[language]; }
    getWeekDaysName(language: string): string[] { return this._weekDayNames[language]; }
    getMonthDaysCount(): number[] { return this._monthDaysCount; }
    getStartDayOfWeek(year: number,month: number): number { return new Date(year,month - 1,1).getDay(); }
    getDayOfWeek(date: DateTime) { return date.getDayOfWeek(); }
    today() { return DateTime.now; }
}