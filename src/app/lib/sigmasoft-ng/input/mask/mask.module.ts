import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { MaskDirective } from "./mask.directive";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [MaskDirective],
  exports: [MaskDirective]
})
export class InputMaskModule {}
