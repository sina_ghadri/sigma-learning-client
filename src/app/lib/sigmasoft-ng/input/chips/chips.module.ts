import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { ChipsComponent } from "./chips.component";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [ChipsComponent],
  exports: [ChipsComponent]
})
export class InputChipsModule {}
