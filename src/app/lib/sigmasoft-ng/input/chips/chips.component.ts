import { Component, OnInit, forwardRef, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ss-input-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.css'],
  providers: [
      {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => ChipsComponent),
          multi: true
      }
  ]
})
export class ChipsComponent implements OnInit, ControlValueAccessor {
  constructor(private element: ElementRef) { }

  onchange: (values: string[]) => any = values => {};
  ontouched: () => any = () => {};
  isdisabled: boolean;

  value: string;
  items: string[];

  ngOnInit() {
  }
  writeValue(values: string[]): void {
    this.items = values;
    if(!this.items) this.items = [];
    this.onchange(values);
  }
  registerOnChange(fn: any): void {
    this.onchange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onchange = fn;
  }
  setDisabledState?(isdisabled: boolean): void {
    this.isdisabled = isdisabled
  }
  addOrRemove() {
    if(this.value && this.value.length)
    {
      for(let i = 0,length = this.items.length;i < length;i++) if(this.value == this.items[i]) return;
      this.items.push(this.value);
      this.value = '';
    }
  }
  remove(item: string) {
    for(let i = 0,length = this.items.length;i < length;i++) if(item == this.items[i]) { this.items.splice(i,1); break; }
  }
  focus() {
    let input = this.element.nativeElement.querySelector('input')
    if(input) input.focus();
  }
}
