import { Pipe, PipeTransform } from "@angular/core";
import { Formatter } from "../../../../sigmasoft-ts/formatter";

@Pipe({
  name: "seperate"
})
export class SeperatePipe implements PipeTransform {
  transform(value: any, ...args: any[]) {
    return Formatter.seperate(value, args[0], args[1]);
  }
}
