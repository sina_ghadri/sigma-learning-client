import { OnInit, Input, ElementRef, HostListener, EventEmitter, Output, Directive } from "@angular/core";
import { NgModel } from "@angular/forms";

@Directive({
  selector: '[ss-input-password]',
  providers: [NgModel]
})
export class PasswordDirective implements OnInit {
	@Output() password: EventEmitter<number> = new EventEmitter;

	@HostListener('ngModelChange',['$event']) Input(event) {
		let level = 0;
		let value: string = this.model.value;
		console.log(value);
		
		if(/.{8,}/.test(value)) level++;
		if(/[a-z].*[a-z]/.test(value)) level++;
		if(/[A-Z].*[A-Z]/.test(value)) level++;
		if(/[0-9].*[0-9]/.test(value)) level++;
		if(/\w.*\w/.test(value)) level++;
		this.password.emit(level);
	}

	constructor(private element: ElementRef,private model: NgModel) {}

	ngOnInit() {
		
	}
}
