import { Directive, OnInit, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
    selector: 'input[ss-input-next]',
})
export class NextDirective implements OnInit, OnChanges {
    @Input('next') pattern: string;

    regex: RegExp;
    public constructor(private element: ElementRef) { }

    ngOnInit(): void { 
        this.element.nativeElement.addEventListener('keyup',() => {
            if(this.regex.test(this.element.nativeElement.value))
            {
                var inputs = document.body.querySelectorAll('input,select');
                let flag = false;
                for(let i = 0;i < inputs.length;i++)
                {
                    if(flag) 
                    {
                        (<HTMLInputElement>inputs[i]).focus();
                        break;
                    }
                    if(inputs[i] == this.element.nativeElement) flag = true;
                }
            }
        })
    }
    ngOnChanges(changes: SimpleChanges)
    {
        if(changes.pattern) this.regex = new RegExp(this.pattern);
    }
}