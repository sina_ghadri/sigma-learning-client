import { NgModule } from "@angular/core";
import { MiscBlockModule } from "./block";
import { MiscCaptchaModule } from "./captcha";
import { MiscCodeModule } from "./code";

@NgModule({
    imports: [

    ],
    exports: [
        MiscBlockModule,
        MiscCaptchaModule,
        MiscCodeModule,
    ]
})
export class BaseMiscModule {}