import { Injectable, EventEmitter } from '@angular/core';
import { AuthorizationType } from './enums';
import { HttpRequest } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class HttpConfigService {

  host: string;
  port: number;
  token: string;
  authorizationType: AuthorizationType = AuthorizationType.Header;

  on1xx: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on1xxContinue: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on1xxSwitchingProtocols: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on1xxProcessing: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on1xxEarlyHints: EventEmitter<HttpRequest<any>> = new EventEmitter;

  on2xx: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxOk: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxCreated: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxAccepted: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxNonAuthoritativeInformation: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxNoContent: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxResetContent: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxPartialContent: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxMultiStatus: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxAlreadyReported: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on2xxImUsed: EventEmitter<HttpRequest<any>> = new EventEmitter;

  on3xx: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxMultipleChoices: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxMovedPermanently: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxFound: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxSeeOther: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxNotModified: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxUseProxy: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxSwitchProxy: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxTemporaryRedirect: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on3xxPermanentRedirect: EventEmitter<HttpRequest<any>> = new EventEmitter;

  on4xx: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxBadRequest: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxUnAuthorize: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxPaymentRequire: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxForbidden: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxNotFound: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxMethodNotAllowed: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxNotAcceptable: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxProxyAuthenticationRquired: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxRequestTimeout: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxConfilict: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxGone: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxLengthRequired: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxPreconditionFailed: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxPayloadTooLarg: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxUrlTooLarg: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxUnsupportedMediaType: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxRangeNotSatisfiable: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxExpectationFailed: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxImTeapot: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxMisdirectedRequest: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxUnprocessableEntity: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxLocked: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxFailedDependency: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxUpgradeRequired: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxPreconditionRequired: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxTooManyRequest: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on4xxRequestHeaderFieldsTooLarg: EventEmitter<HttpRequest<any>> = new EventEmitter;

  on5xx: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxInternalServerError: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxNotImplemented: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxBadGateway: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxServiceUnavailable: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxGatewayTimeout: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxHttpVersionNotSupported: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxVariantAlsoNegotiates: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxInsufficientStorage: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxLoopDetected: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxNotExtended: EventEmitter<HttpRequest<any>> = new EventEmitter;
  on5xxNetworkAuthenticationRequired: EventEmitter<HttpRequest<any>> = new EventEmitter;

  oncomplete: EventEmitter<HttpRequest<any>> = new EventEmitter;

  constructor() { }

}
