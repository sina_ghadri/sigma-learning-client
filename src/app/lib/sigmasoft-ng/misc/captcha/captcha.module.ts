import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CaptchaComponent } from "./components/captcha.component";
import { HttpClientModule } from "@angular/common/http";
import { CaptchaService } from "./captcha.service";

@NgModule({
  imports: [CommonModule, HttpClientModule],
  declarations: [CaptchaComponent],
  exports: [CaptchaComponent],
  providers: [CaptchaService]
})
export class MiscCaptchaModule {}
