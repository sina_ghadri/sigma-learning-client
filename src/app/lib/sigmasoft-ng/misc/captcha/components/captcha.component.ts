import { Component, OnInit, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { CaptchaService } from '../captcha.service';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { KeyValuePair } from '../../../../sigmasoft-ts';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { ICaptchaService } from '../interfaces';

@Component({
  selector: 'ss-misc-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.scss'],
  providers: [
    CaptchaService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CaptchaComponent),
      multi: true
    }
  ]
})
export class CaptchaComponent implements OnInit, ControlValueAccessor {
  @Input() url: string;
  @Input() height: number = 50;
  @Input() progress: boolean;
  @Input() image: SafeStyle;
  @Input() service: ICaptchaService;

  @Output() onrefresh: EventEmitter<KeyValuePair<string, string>> = new EventEmitter

  value: string;
  onchange: (value: string) => any;
  ontouched: () => any;
  isdisabled: boolean;

  writeValue(value: string): void { this.value = value; }

  registerOnChange(fn: any): void { this.onchange = fn; }
  registerOnTouched(fn: any): void { this.ontouched = fn; }
  setDisabledState?(isdisabled: boolean): void { this.isdisabled = isdisabled; }

  constructor(private captchaService: CaptchaService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    if (!this.service) this.service = this.captchaService;
    this.refresh();
  }
  refresh() {
    if (!this.progress) {
      this.progress = true;
      this.service.get(this.url).subscribe(op => {
        this.value = op.data.key;
        if(this.onchange) this.onchange(this.value);
        this.image = this.sanitizer.bypassSecurityTrustUrl(`data:image/png;base64,${op.data.value}`);
        this.progress = false;
      }, e => {
        console.log(e);
        
        this.progress = false
      }, () => this.progress = false);
    }
  }
}
