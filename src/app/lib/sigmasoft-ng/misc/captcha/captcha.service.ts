import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OperationResult, KeyValuePair } from '../../../sigmasoft-ts';
import { Observable } from 'rxjs';
import { ICaptchaService } from './interfaces';

@Injectable()
export class CaptchaService implements ICaptchaService {

  constructor(private http: HttpClient) { }

  public get(url: string): Observable<OperationResult<KeyValuePair<string, string>>> {
    return this.http.get<OperationResult<KeyValuePair<string, string>>>(url);
  }
}
