import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { Error404Component } from "./components/error-404.component";

@NgModule({
  imports: [FormsModule, CommonModule, RouterModule],
  declarations: [Error404Component],
  exports: [Error404Component]
})
export class Error404Module {}
