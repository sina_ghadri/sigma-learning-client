import { EventEmitter, TemplateRef } from "@angular/core";
import { DataTableRow } from "./row";
import { DataTable } from "./datatable";

export class DataTableColumn {
    private _visible: boolean = true;
    private _search: ColumnSearch;

    public width: string;
    public title: string;
    public field: string;
    public order: number;
    public datatable: DataTable;
    public sortable: boolean = true;
    public template: TemplateRef<any>;
    public class: ((row: DataTableRow) => string) | string;
    public value: (row: DataTableRow) => string;
    public html: (row: DataTableRow) => string;

    public edit: DataTableColumnEdit;

    public onsort: EventEmitter<boolean> = new EventEmitter;
    public onvisibleChange: EventEmitter<boolean> = new EventEmitter;

    get search(): ColumnSearch { return this._search; }
    set search(value: ColumnSearch) { this._search = value; this._search.subscribeOnChange(() => this.datatable.stateChange());  this._search.column = this; }

    constructor(title: string, value: string);
    constructor(title: string, value?: (row: DataTableRow) => string);

    public constructor(title: string, value: string | ((row: DataTableRow) => string)) {
        this.title = title;
        if(typeof value == 'string') {
            this.field = value;
            this.html = this.value = row => row.value[value];
        }
        else {
            this.html = this.value = value;
        }

        this.search = new ColumnSearch;
    }

    get visible(): boolean { return this._visible; }
    set visible(value: boolean) { let flag = (this._visible != value); this._visible = value; if (flag) this.onvisibleChange.emit(value); }

    public getClass(row: DataTableRow): string[] {
        let classes: string[] = [];
        if (!this.visible) classes.push('hide');
        if (this.class) {
            classes.push(typeof this.class == 'string' ? this.class : this.class(row));
        }
        return classes;
    }
    public getValue(row: DataTableRow): string {
        try {
            return this.value(row);
        } catch (ex) { console.error(ex); return 'exception'; }
    }
    public getHtml(row: DataTableRow): string {
        try {
            return this.html(row);
        } catch (ex) { console.error(ex); return 'exception'; }
    }
    public getHeaderClass(datatable: DataTable, index: number): string[] {
        let classes: string[] = [];
        if (this.sortable) classes.push('sortable');
        if (!this.visible) classes.push('hide');
        if (datatable.sort == index) {
            if (datatable.ascending) classes.push('asc');
            else classes.push('desc');
        }
        return classes;
    }
    public getSearchClass(): string[] {
        let classes: string[] = [];
        if (!this.visible) classes.push('hide');
        return classes;
    }
    public getSearchItems(rows: DataTableRow[]): string[] {
        let array: string[] = [];
        for (let i = 0, length = rows.length; i < length; i++) {
            let value = this.getValue(rows[i]);
            if (array.indexOf(value) < 0) array.push(value);
        }
        return array;
    }

    public setWidth(value: string): DataTableColumn { this.width = value; return this; }
    public setSearchable(value: boolean): DataTableColumn { this.search.enable = value; return this; }
    public setSearchTemplate(value: TemplateRef<any>): DataTableColumn { this.search.template = value; return this; }
    public setSortable(value: boolean): DataTableColumn { this.sortable = value; return this; }
    public setVisibe(value: boolean): DataTableColumn { this.visible = value; return this; }
    public setClass(value: ((row: DataTableRow) => string) | string): DataTableColumn { this.class = value; return this; }
    public setValue(value: (row: DataTableRow) => string): DataTableColumn { this.value = value; return this; }
    public setHtml(value: (row: DataTableRow) => string): DataTableColumn { this.html = value; return this; }
    public setSearchType(value: DataTableColumnSearchType): DataTableColumn { this.search.type = value; return this; }
    public setEdit(value: DataTableColumnEdit): DataTableColumn { this.edit = value; return this; }
    public setTemplate(value: TemplateRef<any>): DataTableColumn { this.template = value; return this; }

    public subscribeOnSort(value: (x: boolean) => void): DataTableColumn { this.onsort.subscribe(value); return this; }
    public subscribeOnVisibleChange(value: (x: boolean) => void): DataTableColumn { this.onvisibleChange.subscribe(value); return this; }
}
export enum DataTableColumnSearchType { Text = 0, Select = 1 }
export class ColumnSearch {
    private _value: any;

    public column: DataTableColumn;
    public template: TemplateRef<any>;
    public type: DataTableColumnSearchType = DataTableColumnSearchType.Text;
    public enable: boolean = true;
    public onchange: EventEmitter<string> = new EventEmitter;

    get value(): any { return this._value; }
    set value(value: any) { this._value = value; this.onchange.emit(this._value); }

    clear() { this.value = null; }
    setTemplate(value: TemplateRef<any>): ColumnSearch { this.template = value; return this; }

    public subscribeOnChange(value: (x: boolean) => void): ColumnSearch { this.onchange.subscribe(value); return this; }
}
export enum DataTableColumnEditType { Text = 0, Select = 1 }
export abstract class DataTableColumnEdit {
    public value: any;
    public enable: boolean = true;
    public type: DataTableColumnEditType = DataTableColumnEditType.Text;

    constructor() {}
}
export abstract class DataTableColumnEditInput extends DataTableColumnEdit {
    public name: string;

    public onchange: EventEmitter<any> = new EventEmitter;
    public onfocus: EventEmitter<any> = new EventEmitter;
    public onblur: EventEmitter<any> = new EventEmitter;

    constructor(name: string) {
        super();
        this.name = name;
    }
    
    subscribeOnChange(value: (e: any) => void): DataTableColumnEdit { this.onchange.subscribe(value); return this; }
    subscribeOnFocus(value: (e: any) => void): DataTableColumnEdit { this.onfocus.subscribe(value); return this; }
    subscribeOnBlur(value: (e: any) => void): DataTableColumnEdit { this.onblur.subscribe(value); return this; }
}
export class DataTableColumnEditText extends DataTableColumnEditInput {
    public onkeydown: EventEmitter<any> = new EventEmitter;
    public onkeyup: EventEmitter<any> = new EventEmitter;

    constructor(name: string) {
        super(name);
        this.name = name;
        this.type = DataTableColumnEditType.Text;
    }

    subscribeOnKeydown(value: (e: any) => void): DataTableColumnEdit { this.onkeydown.subscribe(value); return this; }
    subscribeOnKeyup(value: (e: any) => void): DataTableColumnEdit { this.onkeyup.subscribe(value); return this; }
}
export class DataTableColumnEditSelect extends DataTableColumnEditInput {
    public items: any[];
    public keyFunc: (value: any) => number;
    public valueFunc: (value: any) => string;
    public compareWith: (val1: any, val2: any) => boolean = (val1, val2) => { return (!val1 && !val2) || (val1 && val2 && val1.key == val2.key); };

    constructor(name: string, items: any[]) {
        super(name);
        this.name = name;
        this.items = items;
        this.type = DataTableColumnEditType.Select;
    }

    setkeyFunc(value: (x: any) => number): DataTableColumnEditSelect { this.keyFunc = value; return this; }
    setValueFunc(value: (x: any) => string): DataTableColumnEditSelect { this.valueFunc = value; return this; }
}
export class DataTableColumnEditTemplate extends DataTableColumnEdit {
    public template: TemplateRef<any>;

    constructor(template: TemplateRef<any>) {
        super();
        this.template = template;
    }

    setTemplate(value: TemplateRef<any>): DataTableColumnEditTemplate { this.template = value; return this; }
}