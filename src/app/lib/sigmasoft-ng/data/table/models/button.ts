import { EventEmitter } from "@angular/core";

export class DataTableButton {
    public text: string;
    public className: string;
    public icon: string;
    public onclick: EventEmitter<any> = new EventEmitter;
    public isenable: (row: any) => boolean = row => true;

    public constructor(text: string, onclick: () => void, icon: string = '', className: string = '') {
        this.text = text;
        this.icon = icon;
        this.className = className;
        this.onclick.subscribe(onclick);
    }

    setIsEnable(value: (row: any) => boolean): DataTableButton { this.isenable = value; return this; }

    subscribeOnClick(value: () => void): DataTableButton { this.onclick.subscribe(value); return this; }
}