import { DataTableRow, DataTableColumn } from './index';
import { Saver } from '../../../../sigmasoft-ts/saver';

export class DataTableExport {
    public static excel(name:string,rows:DataTableRow[],columns: DataTableColumn[]) {
        let data: any[] = [];
        let array: string[] = [];
        for (let i = 0; i < columns.length; i++) array.push(columns[i].title);
        data.push(array);
        for (let i = 0; i < rows.length; i++) {
            array = [];
            let row = rows[i];
            for (let j = 0; j < columns.length; j++) array.push(columns[j].getValue(row));
            data.push(array);
        }
        Saver.xlsx(data, name);
    }
}