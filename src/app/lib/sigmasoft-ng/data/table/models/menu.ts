import { DataTableRow } from "./row";
import { EventEmitter } from "@angular/core";

export class DataTableMenuItem {
    public text: string;
    public className: string;
    public icon: string;
    public tooltip: string;
    public onclick:EventEmitter<DataTableRow> = new EventEmitter;
    public isenable: (row: DataTableRow) => boolean = row => true;

    public constructor(tooltip: string, onclick: (row: DataTableRow) => void, icon: string = '', className: string = '') {
        this.tooltip = tooltip;
        this.icon = icon;
        this.className = className;
        this.onclick.subscribe(onclick);
    }

    public setIsEnable(value: (row: DataTableRow) => boolean = row => true): DataTableMenuItem { this.isenable = value; return this; }
    public setText(value: string): DataTableMenuItem { this.text = value; return this; }
    public setClassName(value: string): DataTableMenuItem { this.className = value; return this; }
    public setIcon(value: string): DataTableMenuItem { this.icon = value; return this; }
    public setTooltip(value: string): DataTableMenuItem { this.tooltip = value; return this; }

    subscribeOnClick(value: (row: DataTableRow) => void): DataTableMenuItem { this.onclick.subscribe(value); return this;}
}
export enum DataTableMenuType { Default, HoverMenu, HoverInline, DropDown }
export class DataTableMenu {
    public type: DataTableMenuType = DataTableMenuType.Default;
    public items: DataTableMenuItem[] = [];
    public isenable: (row: DataTableRow) => boolean = row => true;

    public getClass() {
        let className = '';
        if (this.items.length == 0) className += 'hide ';
        switch (this.type) {
            case DataTableMenuType.HoverMenu: className += 'hover menu '; break;
            case DataTableMenuType.HoverInline: className += 'hover inline '; break;
            case DataTableMenuType.DropDown: className += 'dropdown '; break;
            case DataTableMenuType.Default: className += 'default '; break;
        }
        return className;
    }
}