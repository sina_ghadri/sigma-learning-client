import { DataTable } from './datatable';
import { EventEmitter } from '@angular/core';

export class DataTableRow {
    private _editing: boolean = false;
    private _collapse: boolean = false;
    private _selected: boolean = false;

    public temp: any;
    public value: any;
    public datatable: DataTable;

    public onselectedChange: EventEmitter<boolean> = new EventEmitter;
    public oneditingChange: EventEmitter<boolean> = new EventEmitter;
    public oncollapseChange: EventEmitter<boolean> = new EventEmitter;

    get selected(): boolean { return this._selected; }
    set selected(value: boolean) { let flag = (value != this._selected); this._selected = value; if(flag) this.onselectedChange.emit(value); }

    get editing(): boolean { return this._editing; }
    set editing(value: boolean) {
        if(value != this._editing)
        {
            let columns = this.datatable.columns;
            for(let i = 0;i < columns.length;i++) {
                if(columns[i].edit) {
                    columns[i].edit.value = columns[i].getValue(this);
                }
            }
        }
        let flag = (this._editing != value);
        this._editing = value; 
        if(flag) this.oneditingChange.emit(value);
    }

    get collapse(): boolean { return this._collapse; }
    set collapse(value: boolean) {
        if(!this.datatable.options.rowOptions.collapseMultiple)
        {
            let rows = this.datatable.rows;
            for(let i = 0,length = rows.length;i < length;i++) rows[i]._collapse = false;
        }
        let flag = (this._collapse != value);
        this._collapse = value;
        if(flag) this.oncollapseChange.emit(value);
    }

    subscribeOnSelectedChange(value: (x: boolean) => void): DataTableRow { this.onselectedChange.subscribe(value); return this; }
    subscribeOnCollapseChange(value: (x: boolean) => void): DataTableRow { this.oncollapseChange.subscribe(value); return this; }
    subscribeOnEditingChange(value: (x: boolean) => void): DataTableRow { this.oneditingChange.subscribe(value); return this; }

    getClass(): string[] {
        let classes: string[] = [];
        if(this.collapse) classes.push('collapse');
        if(this.selected) classes.push('selected');
        if(this.editing) classes.push('editing');
        return classes;
    }
}
