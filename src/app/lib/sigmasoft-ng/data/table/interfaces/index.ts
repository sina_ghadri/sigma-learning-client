import { DataTable } from "../models";

export interface IDataTableService {
    get(datatable: DataTable) : { count: number, data: any[] };
}