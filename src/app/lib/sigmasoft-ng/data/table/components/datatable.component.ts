import { Component, OnInit, Input, EventEmitter, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';

import { DataTableRow, DataTable, DataTableColumn } from "../models";
import { DataTableExport } from "../models/export";
import { JDate } from '../../../../sigmasoft-ts';
import { Modal } from '../../../overlay';

@Component({
    selector: 'ss-data-table',
    templateUrl: './datatable.component.html',
    styleUrls: ['./datatable.component.scss']
})
export class DataTableComponent implements OnInit {
    @Input() datatable: DataTable;
    @Input('collapse-template') collapseTemplate: TemplateRef<any>;
    @Input('min-height') minHeight: string = 'auto';

    selectedColumn: DataTableColumn;

    collapseObj: any = {};

    onrowClick: EventEmitter<DataTableRow> = new EventEmitter;
    onrowDoubleClick: EventEmitter<DataTableRow> = new EventEmitter;
    onrowSelectedChange: EventEmitter<DataTableRow> = new EventEmitter;
    onrefresh: EventEmitter<any> = new EventEmitter;

    columnsModal: Modal = new Modal;

    public constructor(private route: ActivatedRoute, private hotkeysService: HotkeysService) { }
    ngOnInit(): void {
        if(this.datatable) {
            if(this.datatable.columns.length > 0) this.selectedColumn = this.datatable.columns[0];
        }
        if(this.collapseTemplate) this.datatable.collapse.template = this.collapseTemplate;

        this.datatable.loadOptions();
        this.route.queryParams.subscribe(params => {
            if (+params['page']) this.datatable.page = +params['page'];
            if (+params['sort']) this.datatable.sort = +params['sort'];
        });

        this.hotkeysService.add(new Hotkey('left', (event: KeyboardEvent): boolean => { this.datatable.nextPage(); return false; }));
        this.hotkeysService.add(new Hotkey('right', (event: KeyboardEvent): boolean => { this.datatable.prevPage(); return false; }));

        this.onrowClick.subscribe(data => this.datatable.onrowClick.emit(data));
        this.onrowDoubleClick.subscribe(data => this.datatable.onrowDoubleClick.emit(data));
        this.onrowSelectedChange.subscribe(data => this.datatable.onrowSelectedChange.emit(data));
        this.onrefresh.subscribe(data => this.datatable.onrefresh.emit(data));

        this.datatable.onrowCollapse.subscribe((row: DataTableRow) => this.collapseObj = Object.assign({}, row.value));

        this.datatable.stateChange();
    }

    columnsSetting() {

    }
    excel() {
        let rows = this.datatable.rowsSelected;
        if (rows.length == 0) rows = this.datatable.rowsResult;
        DataTableExport.excel(this.datatable.name + JDate.format(new Date(), 'yyyy/MM/dd hh:mm:ss') + '.xlsx', rows, this.datatable.columns);
        this.datatable.onexport.emit();
    }
}
