import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OrderlistComponent } from "./orderlist.component";

@NgModule({
  imports: [CommonModule],
  declarations: [OrderlistComponent],
  exports: [OrderlistComponent]
})
export class DataOrderlistModule {}
