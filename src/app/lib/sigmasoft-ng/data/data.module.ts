import { NgModule } from "@angular/core";
import { DataCarouselModule } from "./carousel";
import { DataOrderlistModule } from "./orderlist";
import { DataPaginatorModule } from "./paginator";
import { DataPicklistModule } from "./picklist";
import { DataSchedulerModule } from "./scheduler";
import { DataScrollerModule } from "./scroller";
import { DataTableModule } from "./table";
import { DataTreetableModule } from "./treetable";
import { DataTreeviewModule } from "./treeview";

@NgModule({
  imports: [],
  exports: [
    DataCarouselModule,
    DataOrderlistModule,
    DataPaginatorModule,
    DataPicklistModule,
    DataSchedulerModule,
    DataScrollerModule,
    DataTableModule,
    DataTreetableModule,
    DataTreeviewModule
  ]
})
export class BaseDataModule {}
