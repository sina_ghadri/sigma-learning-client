import { LoginInfo, Module } from "../models";
import { Observable } from "rxjs";

export interface IPanelAdminManagerService {
    symbol: string;
    activeModule: Module;
    loginInfo: LoginInfo;

    sendLoginInfo(link: string, obj: LoginInfo): Observable<any>;
}