import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { PanelAdminComponent } from "./components/panel.component";

@NgModule({
  imports: [FormsModule, CommonModule, RouterModule],
  declarations: [PanelAdminComponent],
  exports: [PanelAdminComponent]
})
export class PanelAdminModule {}
