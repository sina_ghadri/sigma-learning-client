export class Menu {
    id: number;
    order: number;
    name: string;
    link: string;
    icon: string;
    badge: string;

    parent: Menu;
    childrens: Menu[] = [];

    isopen: boolean;

    constructor(menu: Menu = null)
    {
        if(menu)
        {
            this.id = menu.id;
            this.name = menu.name;
            this.link = menu.link;
            this.icon = menu.icon;
            this.badge = menu.badge;
        }
    }

    open() {
        this.isopen = true;
    }
    close() {
        for(let i = 0;i < this.childrens.length;i++)
            this.childrens[i].close();
        this.isopen = false;
    }
    toggle() {
        if(this.isopen) this.close();
        else this.open();
    }
}