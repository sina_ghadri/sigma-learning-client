export class User {
  id: number;
  name: string;
  username: string;
  password: string;
  nationalId: string;
  birthday: string;
  cellphone: string;
  email: string;
  gender: string;

  telephone: string;
  fax: string;

  telegram: string;
  instagram: string;
  whatsApp: string;
  twitter: string;
  linkedIn: string;
  
  description: string;

  image: string;
}
