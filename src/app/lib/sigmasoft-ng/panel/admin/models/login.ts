import { User } from "./user";

export class PageLogin {
  user: User = new User;
  background: string;
  backgroundStyle: any;
  logo: string;
  logoStyle: any;
  title: string;
  symbol: string;
  description: string;
  message: string;
  captcha: string;
  progress: boolean;

  setUser(value: User): PageLogin { this.user = value; return this; }
  setBackground(value: string): PageLogin { this.background = value; return this; }
  setBackgroundStyle(value: any): PageLogin { this.backgroundStyle = value; return this; }
  setLogo(value: string): PageLogin { this.logo = value; return this; }
  setLogoStyle(value: any): PageLogin { this.logoStyle = value; return this; }
  setTitle(value: string): PageLogin { this.title = value; return this; }
  setSymbol(value: string): PageLogin { this.symbol = value; return this; }
  setDescription(value: string): PageLogin { this.description = value; return this; }
}