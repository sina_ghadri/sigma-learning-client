import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordingComponent } from './according.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AccordingComponent],
  exports: [AccordingComponent]
})
export class PanelAccordingModule { }
