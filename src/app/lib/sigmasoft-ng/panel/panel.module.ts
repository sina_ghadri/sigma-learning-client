import { NgModule } from "@angular/core";
import { PanelAccordingModule } from "./according";
import { PanelAdminModule } from "./admin";
import { PanelCardModule } from "./card";
import { PanelFieldsetModule } from "./fieldset";
import { PanelPanelModule } from "./panel";
import { PanelScrollModule } from "./scroll";
import { PanelTabModule } from "./tab";
import { PanelToolbarModule } from "./toolbar";
import { PanelBoardModule } from "./board";

@NgModule({
  imports: [],
  exports: [
    PanelAccordingModule,
    PanelAdminModule,
    PanelBoardModule,
    PanelCardModule,
    PanelFieldsetModule,
    PanelPanelModule,
    PanelScrollModule,
    PanelTabModule,
    PanelToolbarModule
  ]
})
export class BasePanelModule {}
