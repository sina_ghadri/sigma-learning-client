import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MegaComponent } from './mega.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MegaComponent]
})
export class MegaModule { }
