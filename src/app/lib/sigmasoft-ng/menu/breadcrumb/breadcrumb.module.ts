import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { BreadcrumbComponent } from "./components/breadcrumb.component";
import { RouterModule } from "@angular/router";

@NgModule({
  imports: [FormsModule, CommonModule, RouterModule],
  declarations: [BreadcrumbComponent],
  exports: [BreadcrumbComponent]
})
export class MenuBreadcrumbModule {}
