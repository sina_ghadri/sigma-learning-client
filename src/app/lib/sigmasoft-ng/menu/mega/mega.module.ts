import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MegaComponent } from "./mega.component";

@NgModule({
  imports: [CommonModule],
  declarations: [MegaComponent],
  exports: [MegaComponent]
})
export class MenuMegaModule {}
