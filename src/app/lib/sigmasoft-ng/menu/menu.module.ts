import { NgModule } from "@angular/core";
import { MenuBarModule } from "./bar";
import { MenuBreadcrumbModule } from "./breadcrumb";
import { MenuContextModule } from "./context";
import { MenuMenuModule } from "./menu";
import { MenuPanelModule } from "./panel";
import { MenuSlideModule } from "./slide";
import { MenuStepModule } from "./step";
import { MenuMegaModule } from "./mega";

@NgModule({
  imports: [],
  exports: [
    MenuBarModule,
    MenuBreadcrumbModule,
    MenuContextModule,
    MenuMegaModule,
    MenuMenuModule,
    MenuPanelModule,
    MenuSlideModule,
    MenuStepModule
  ]
})
export class BaseMenuModule {}
