import { NgModule } from "@angular/core";
import { ProgressBarModule } from "./bar";
import { ProgressSpinnerModule } from "./spinner";

@NgModule({
    imports: [

    ],
    exports: [
        ProgressBarModule,
        ProgressSpinnerModule
    ]
})
export class BaseProgressModule {}