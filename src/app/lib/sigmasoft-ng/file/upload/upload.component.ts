import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'

@Component({
  selector: 'ss-file-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  @Input() url: string = 'http://localhost';

  files: any[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  onFileChange(event) {
    let files = event.target.files;
    if(files) {
      for(let i = 0,length = files.length;i < length;i++) {
        let file = files[i];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.files.push({
            name: file.name,
            type: file.type,
            size: file.size,
            status: 'Ready',
            value: reader.result
          })
        };
      }
    }
  }
  getFormattedSize(size: number) {
    if(size > 1000000000) return (size / 1000000000).toFixed(2) + ' GB';
    if(size > 1000000) return (size / 1000000).toFixed(2) + ' MB';
    if(size > 1000) return (size / 1000).toFixed(2) + ' KB';
    return size + ' Byte'
  }
  getFormattedTime(time: number) {
    if(!time) return '__ __';
    let tmp = '';
    if(time > 3600) tmp += Math.floor(time/3600) + ' h ';
    if(time > 60) tmp += Math.floor((time%3600)/60) + ' m ';
    return tmp + Math.floor(time%60) + ' s';
  }
  getFormattedSpeed(speed: number) {
    if(!speed) return '__';
    if(speed > 1000) return (speed / 1000).toFixed(1) + ' KB/s';
    return speed + ' Byte/s'
  }
  getSegments(file: any): any[] {
    let segments = [];
    let size = 500000;
    for(let i = 0;i < file.value.length;i+=size) {
      segments.push({
        start: i,
        value: file.value.substr(i,size),
        attempt: -1,
        status: 'Ready',
        file: file,
        segments: segments
      });
    }
    for(let i = 0,length = segments.length - 1;i < length;i++) segments[i].next =segments[i + 1];
    return segments;
  }
  upload(file: any,count: number = 4) {
    if(!file) return;
    if(file.status == 'Cancel') { this.upload(file.next); return; }
    if(!file.segments) file.segments = this.getSegments(file);
    file.status = 'Uploading';
    if(file.segments.length > 0) this.send(file.segments[0]);
  }
  send(segment: any) {
    if(segment.status == 'Failed') {
      if(segment.attempt < 10) {
        segment.attempt++;
        segment.status = 'Uploading';
      } else { segment.file.status = 'Cancel'; this.upload(segment.file.next); return; }
    }
    else if(segment.status == 'Ready') {
      segment.status = 'Uploading';
    }
    segment.start = new Date().getTime();
    this.http.post(this.url,segment.value).subscribe(() => {
      segment.status = 'Complete';
      if(segment.next) this.send(segment.next);
      else this.upload(segment.file.next);
    },e => {
      segment.status = 'Failed';
      if(segment.next) this.send(segment.next);
      else this.upload(segment.file.next);
    });
  }
  calculate(segment: any) {
    segment.file.progress = Math.floor(segment.segments.filter(e => e.status == 'Complete').length*100/segment.segments.length);

  }
  uploadAll() {
    for(let i = 0,length = this.files.length - 1;i < length;i++) this.files[i].next = this.files[i + 1];
    this.upload(this.files[0]);
  }

  cancel(file: any) {}
  cancelAll() {
    for(let i = 0,length = this.files.length;i < length;i++) this.files[i].status = 'Cancel';
  }

  remove(file: any) { 
    this.cancel(file); 
    for(let i = 0,length = this.files.length;i < length;i++) if(file.name == this.files[i].name) { this.files.splice(i,1); break; }
  }
  removeAll() { this.cancelAll(); this.files = []; }
  removeComplete() {}

  uploadDisabled(): boolean { return this.files.filter(e => e.status == 'Ready' || e.status == 'Puase').length == 0; }
  cancelDisabled(): boolean { return this.files.filter(e => e.status == 'Uploading').length == 0; }
  removeDisabled(): boolean { return this.files.length == 0; }
  removeCompleteDisabled(): boolean { return this.files.filter(e => e.status == 'Complete').length == 0; }
}
