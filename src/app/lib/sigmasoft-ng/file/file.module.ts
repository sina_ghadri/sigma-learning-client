import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { FileUploadModule } from "./upload";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [],
  exports: [FileUploadModule]
})
export class BaseFileModule {}
