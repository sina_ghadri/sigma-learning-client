import { NgModule } from "@angular/core";
import { DragDropModule } from "./drag-drop";

@NgModule({
  imports: [],
  exports: [DragDropModule]
})
export class BaseDragDropModule {}
