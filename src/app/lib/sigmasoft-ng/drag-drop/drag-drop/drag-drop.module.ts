import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DragDropComponent } from "./drag-drop.component";

@NgModule({
  imports: [CommonModule],
  declarations: [DragDropComponent],
  exports: [DragDropComponent]
})
export class DragDropModule {}
