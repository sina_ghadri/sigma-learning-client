import { NgModule } from "@angular/core";
import { OverlayLightboxModule } from "./lightbox";
import { OverlayModalModule } from "./modal";
import { OverlaySidebarModule } from "./sidebar";
import { OverlayPanelModule } from "./panel";
import { OverlayTooltipModule } from "./tooltip";

@NgModule({
    imports: [

    ],
    exports: [
        OverlayLightboxModule,
        OverlayModalModule,
        OverlaySidebarModule,
        OverlayPanelModule,
        OverlayTooltipModule,
    ]
})
export class BaseOverlayModule {}