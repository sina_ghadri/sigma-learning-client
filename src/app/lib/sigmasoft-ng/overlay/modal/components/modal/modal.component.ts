import { Component, Input, OnInit, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { Modal } from '../../models';

@Component({
    selector: 'ss-overlay-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    @Input() modal: Modal;
    @Input('modal-title') title: string;
    @Input('min-width') minWidth: string;

    @Output() onclose: EventEmitter<any> = new EventEmitter();
    @Output() onoverlayClick: EventEmitter<any> = new EventEmitter();

    @HostListener('click',['$event']) onclick() {
        if(!this.element.nativeElement.querySelector('.ss-overlayer>.ss-dialog:hover')) this.onoverlayClick.emit();
    }
    @HostListener('mousemove',['$event']) onmousemove(e: MouseEvent) {
        if(this.dragging) {
            this.top = e.clientY - this.offsetY;
            this.left = e.clientX - this.offsetX;
        }
    }

    dragging: boolean;

    private top: number;
    private left: number;

    private offsetX: number;
    private offsetY: number;

    attachLeft: boolean;
    attachRight: boolean;
    attachTop: boolean;
    attachBottom: boolean;

    private hotKey: Hotkey;

    get getStyles() : any {
        let styles: any = {'min-width': this.minWidth};
        if(this.top) styles.top = this.top + 'px';
        if(this.left) styles.left = this.left + 'px';
        if(this.attachTop) styles.top = 0; 
        if(this.attachRight) styles.right = 0; 
        if(this.attachBottom) styles.bottom = 0; 
        if(this.attachLeft) styles.left = 0; 
        return styles;
    }

    constructor(private element: ElementRef,private hotkeysService: HotkeysService) {}

    ngOnInit(): void {
        if(this.modal) {
            this.modal.onchange = (isopen: boolean) => {
                if(isopen) this.setKeys();
                else this.removeKeys();
            }
        }
    }
    setKeys() {
        if(!this.hotKey) {
            this.hotKey = new Hotkey('esc', (event: KeyboardEvent): boolean => { this.close(); return false; });
            this.hotkeysService.add(this.hotKey);
        }
    }
    removeKeys() {
        if(this.hotKey) {
            this.hotkeysService.remove(this.hotKey);
            this.hotKey = undefined;
        }
    }

    close() {
        this.modal.close();
        this.onclose.emit();
    }
    dragStart(e: MouseEvent) {
        let header = this.element.nativeElement.getElementsByClassName('ss-header')[0];
        let offset = header.getBoundingClientRect();
        this.offsetX = e.clientX - offset.left;
        this.offsetY = e.clientY - offset.top;
        this.dragging = true;
    }
    dragEnd(e: MouseEvent) {
        this.dragging = false;
    }
    fullscreen() {
        if(this.attachTop && this.attachRight && this.attachBottom && this.attachLeft) {
            this.attachTop = false;
            this.attachRight = false;
            this.attachBottom = false;
            this.attachLeft = false;
        } else {
            this.attachTop = true;
            this.attachRight = true;
            this.attachBottom = true;
            this.attachLeft = true;
        }
    }
    halignCenter() {
        if(this.attachTop && this.attachBottom) {
            this.attachTop = false;
            this.attachBottom = false;
        } else {
            this.attachTop = true;
            this.attachBottom = true;
        }
    }
    valignCenter() {
        if(this.attachRight && this.attachLeft) {
            this.attachRight = false;
            this.attachLeft = false;
        } else {
            this.attachRight = true;
            this.attachLeft = true;
        }
    }
}