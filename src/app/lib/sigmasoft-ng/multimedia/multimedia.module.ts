import { NgModule } from "@angular/core";
import { MultimediaGalleryModule } from "./gallery";

@NgModule({
    imports: [

    ],
    exports: [
        MultimediaGalleryModule
    ]
})
export class BaseMultimediaModule {}