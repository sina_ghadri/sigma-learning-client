export class MultimediaGalleryItem {
    title: string;
    description: string;
    src: string;
}